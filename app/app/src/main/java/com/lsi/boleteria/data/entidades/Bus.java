/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.data.entidades;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Juan Carlos
 */

public class Bus implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Sucursal sucursal;
    private String nombre;
    private Integer cantidadAsiento;
    private String numeroPlaca;
    private String conductor;
    private List<Horario> horarioList;
    private List<Asiento> asientoList;
    private String urlImagenBus;

    public Bus() {
    }

    public Bus(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCantidadAsiento() {
        return cantidadAsiento;
    }

    public void setCantidadAsiento(Integer cantidadAsiento) {
        this.cantidadAsiento = cantidadAsiento;
    }

    public String getNumeroPlaca() {
        return numeroPlaca;
    }

    public void setNumeroPlaca(String numeroPlaca) {
        this.numeroPlaca = numeroPlaca;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public List<Horario> getHorarioList() {
        return horarioList;
    }

    public void setHorarioList(List<Horario> horarioList) {
        this.horarioList = horarioList;
    }

    public List<Asiento> getAsientoList() {
        return asientoList;
    }

    public void setAsientoList(List<Asiento> asientoList) {
        this.asientoList = asientoList;
    }

    public String getUrlImagenBus() {
        return urlImagenBus;
    }

    public void setUrlImagenBus(String urlImagenBus) {
        this.urlImagenBus = urlImagenBus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bus)) {
            return false;
        }
        Bus other = (Bus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lsi.boleteria.data.entidades.Bus[ id=" + id + " ]";
    }
    
}
