package com.lsi.boleteria.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lsi.boleteria.R;
import com.lsi.boleteria.data.models.UsuarioModel;
import com.lsi.boleteria.data.services.Services;
import com.lsi.boleteria.util.Constantes;

public class OlvidoContraseniaActivity extends AppCompatActivity {

    EditText editTextNombreUsuario;
    Button btnSiguienteOlvidoContra;
    private ProgressDialog progressDialog;
    private Services services;
    UsuarioModel usuarioModel;
    Boolean disponible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        setContentView(R.layout.activity_olvido_contrasenia);
        Constantes.hideKeyboardV2(this);
        Toolbar toolbar = findViewById(R.id.toolbarOlvidoContrasenia);
        setSupportActionBar(toolbar);
        services = new Services();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        disponible = Boolean.FALSE;

        TextView txtEnvioCorreo = findViewById(R.id.txtEnvioCorreo);
        Button btnEnviarCorreo = findViewById(R.id.btnEnviarCorreo);
        btnEnviarCorreo.setVisibility(disponible ? View.VISIBLE : View.GONE);
        txtEnvioCorreo.setVisibility(disponible ? View.VISIBLE : View.GONE);

        editTextNombreUsuario = findViewById(R.id.editTextNombreUsuario);
        btnSiguienteOlvidoContra = findViewById(R.id.btnSiguienteCorreo);
        btnSiguienteOlvidoContra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextNombreUsuario.getText() == null) {
                    Toast.makeText(OlvidoContraseniaActivity.this, "Debe ingresar un nombre de Usuario", Toast.LENGTH_SHORT).show();
                } else {

                    String nombreUsuario = editTextNombreUsuario.getText().toString();
                    new AsyncTask<Void, Void, Boolean>() {
                        @Override
                        protected Boolean doInBackground(Void... params) {
                            try {
                                usuarioModel = services.existeUsuario(nombreUsuario);
                                if (usuarioModel == null) {
                                    disponible = Boolean.FALSE;
                                    return false;
                                }
                                if (usuarioModel.getIdUsuario() == null) {
                                    disponible = Boolean.FALSE;
                                    return false;
                                }
                                disponible = Boolean.TRUE;
                                return true;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return false;
                        }

                        @Override
                        protected void onPostExecute(Boolean result) {
                            progressDialog.dismiss();
                            if (result) {
                                btnEnviarCorreo.setVisibility(disponible ? View.VISIBLE : View.GONE);
                                txtEnvioCorreo.setVisibility(disponible ? View.VISIBLE : View.GONE);
                                txtEnvioCorreo.setText("Se enviará un correo electrónico a la siguiente dirección: " + usuarioModel.getCorreo());
                            } else {
                                noDisponible();
                            }
                        }

                        @Override
                        protected void onProgressUpdate(Void... values) {
                            super.onProgressUpdate(values);
                        }

                        @Override
                        protected void onPreExecute() {
                            progressDialog.setMax(100);
                            progressDialog.setMessage("Validando Usuario ...");
                            // progressDialog.setTitle(R.string.iniciando_sesion);
                            progressDialog.setCancelable(false);
                            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressDialog.show();
                            super.onPreExecute();
                        }
                    }.execute();
                }
            }
        });


        btnEnviarCorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... params) {
                        try {
                            services.recuperarContrasenia(usuarioModel.getIdUsuario());
                            return true;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        progressDialog.dismiss();
                        envio();
                    }

                    @Override
                    protected void onProgressUpdate(Void... values) {
                        super.onProgressUpdate(values);
                    }

                    @Override
                    protected void onPreExecute() {
                        progressDialog.setMax(100);
                        progressDialog.setMessage("Validando Usuario ...");
                        // progressDialog.setTitle(R.string.iniciando_sesion);
                        progressDialog.setCancelable(false);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();
                        super.onPreExecute();
                    }
                }.execute();


            }
        });


    }

    private void noDisponible() {
        Toast.makeText(this, "Usuario no se encuentra registrado", Toast.LENGTH_SHORT).show();
    }

    private void envio() {
        Toast.makeText(this, "Correo enviado correctamente", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
