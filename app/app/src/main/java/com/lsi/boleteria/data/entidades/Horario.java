/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.data.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Horario implements Serializable {

    private Long idHorario;
    private Bus bus;
    private String horaDescripcion;
    private String fechaSalida;
    private String destino;
    private String sucursal;
    private String urlDestinoImagen;
    private List<Asiento> asientoList;


    public Horario(Long idHorario, Bus bus, String horaDescripcion, String fechaSalida,
                   String destino, String urlDestinoImagen) {
        this.idHorario = idHorario;
        this.bus = bus;
        this.horaDescripcion = horaDescripcion;
        this.fechaSalida = fechaSalida;
        this.destino = destino;
        this.urlDestinoImagen = urlDestinoImagen;
    }

    public Horario() {
    }

    public Long getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Long idHorario) {
        this.idHorario = idHorario;
    }

    public Bus getBus() {
        if (bus == null)
            bus = new Bus();
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public String getHoraDescripcion() {
        return horaDescripcion;
    }

    public void setHoraDescripcion(String horaDescripcion) {
        this.horaDescripcion = horaDescripcion;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getUrlDestinoImagen() {
        return urlDestinoImagen;
    }

    public void setUrlDestinoImagen(String urlDestinoImagen) {
        this.urlDestinoImagen = urlDestinoImagen;
    }

    public List<Asiento> getAsientoList() {
        return asientoList;
    }

    public void setAsientoList(List<Asiento> asientoList) {
        this.asientoList = asientoList;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

}
