package com.lsi.boleteria.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lsi.boleteria.R;
import com.lsi.boleteria.activities.CompraBoletoActivity;
import com.lsi.boleteria.data.entidades.Boleto;
import com.lsi.boleteria.data.entidades.Horario;
import com.lsi.boleteria.data.models.BoletoResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BoletosAdapter extends BaseAdapter {


    private Context context;
    private ArrayList<BoletoResponse> boletoResponses;

    public BoletosAdapter(Context context, ArrayList<BoletoResponse> boletoResponses) {

        this.context = context;
        this.boletoResponses = boletoResponses;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {
        if (boletoResponses != null)
            return boletoResponses.size();
        else
            return 0;
    }

    @Override
    public BoletoResponse getItem(int position) {
        return boletoResponses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BoletosAdapter.ViewHolder holder;

        if (convertView == null) {
            holder = new BoletosAdapter.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.card_boleto, null, true);

            holder.txtDestino = convertView.findViewById(R.id.txtDestino);
            holder.txtPropietario = convertView.findViewById(R.id.txtPropietario);
            holder.txtFecha = convertView.findViewById(R.id.txtFecha);
            holder.txtAsiento = convertView.findViewById(R.id.txtAsiento);
            holder.txtValor = convertView.findViewById(R.id.txtValor);
            holder.txtBusDescripcion = convertView.findViewById(R.id.txtBusDescripcion);
            convertView.setTag(holder);
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (BoletosAdapter.ViewHolder) convertView.getTag();
        }

        //Picasso.get().load(boletoResponses.get(position).getBus().getUrlImagenBus()).into(holder.imgUrlBus);

        holder.txtDestino.setText("Destino: " + boletoResponses.get(position).getDestino());
        holder.txtPropietario.setText("Pasajero: " + boletoResponses.get(position).getNumeroBoletoFormato() + "-" + boletoResponses.get(position).getUsuarioEmite());

        holder.txtFecha.setText("Fecha: " + " " + boletoResponses.get(position).getFechaSalida());
        holder.txtAsiento.setText("Asiento:" + " " + boletoResponses.get(position).getAsiento());
        holder.txtValor.setText("Valor: $" + " " + boletoResponses.get(position).getTotal());
        holder.txtBusDescripcion.setText("Boleto: " + boletoResponses.get(position).getNumeroBoleto().toString() +
                "- Anden 48 SP1 - Bus: " + boletoResponses.get(position).getBus());
        holder.txtBusDescripcion.setTextColor(Color.RED);

        /*holder.btnComprar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(context, CompraBoletoActivity.class);
                        intent.putExtra("HORARIO", horariosList.get(position));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                }
        );
*/

        return convertView;
    }

    private class ViewHolder {

        protected TextView txtFecha, txtBusDescripcion, txtDestino, txtAsiento, txtValor, txtPropietario;

    }


}
