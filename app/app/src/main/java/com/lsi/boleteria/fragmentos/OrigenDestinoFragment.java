package com.lsi.boleteria.fragmentos;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lsi.boleteria.R;
import com.lsi.boleteria.activities.CompraBoletoActivity;
import com.lsi.boleteria.activities.HorarioActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class OrigenDestinoFragment extends Fragment implements View.OnClickListener {


    private Spinner spDestino, spOrigen;
    private Button consultarHorario;
    private EditText etPlannedDate;
    private DateFormat formatter;
    private Date todayWithZeroTime;
    private Integer idDestino = 0;
    private Double valor = 1.80;

    private Integer positionOrigen, positionDestino;

    @SuppressLint("SimpleDateFormat")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_origen_destino, container, false);
        positionOrigen = 0;
        positionDestino = 0;
        spDestino = view.findViewById(R.id.spDestino);
        spOrigen = view.findViewById(R.id.spOrigen);
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        listarOrigenes();

        spOrigen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0 || i == 1) {
                    listarTTGTP();
                }
                if (i == 2) {
                    listarTP();
                }
                if (i == 3) {
                    listarTV();
                }
                positionOrigen = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        TextView tvValor = view.findViewById(R.id.tvValor);

        spDestino.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                positionDestino = i;
                destino();
                tvValor.setText("Valor Incluye Anden $ " + valor.toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        consultarHorario = view.findViewById(R.id.consultarHorario);
        consultarHorario.setOnClickListener(this);
        etPlannedDate = view.findViewById(R.id.etPlannedDate);
        etPlannedDate.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.etPlannedDate:
                showDatePickerDialog();
                break;
            case R.id.consultarHorario:
                if (etPlannedDate.getText() != null && etPlannedDate.getText().toString() != null
                        && !etPlannedDate.getText().toString().equals("")) {

                    try {
                        todayWithZeroTime = formatter.parse(etPlannedDate.getText().toString());
                        Log.d("todayWithZeroTime", ">>" + todayWithZeroTime);
                        Log.d("todayWithZegetTime()", ">>" + todayWithZeroTime.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    destino();
                    Intent intent = new Intent(getContext(), HorarioActivity.class);
                    intent.putExtra("DESTINO", idDestino);
                    intent.putExtra("VALOR", valor);
                    intent.putExtra("FECHA", todayWithZeroTime.getTime());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);

                } else {
                    Toast.makeText(getContext(), "Debe Ingresar los Campos Obligatorios", Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    public void destino() {
        Double anden = 0.25;
        switch (positionOrigen) {
            case 0: //GUAYAQUIL
                switch (positionDestino) {
                    case 0: //STA LUCIA
                        idDestino = 1;
                        valor = 1.80;
                        break;
                    case 1: //PETRILLO
                        idDestino = 2;
                        valor = 1.35;
                        break;
                    case 2: //PALESTINA
                        idDestino = 3;
                        valor = 2.00;
                        break;
                    case 3: //VINCES
                        idDestino = 4;
                        valor = 3.15;
                        break;
                    case 4: //PALENQUE
                        idDestino = 5;
                        valor = 3.80;
                        break;
                }
                break;
            case 1: //PASCUALES
                switch (positionDestino) {
                    case 0: //STA LUCIA
                        idDestino = 6;
                        valor = 1.80;
                        break;
                    case 1: //PETRILLO
                        idDestino = 7;
                        valor = 1.35;
                        break;
                    case 2: //PALESTINA
                        idDestino = 8;
                        valor = 2.00;
                        break;
                    case 3: //VINCES
                        idDestino = 9;
                        valor = 3.15;
                        break;
                    case 4: //PALENQUE
                        idDestino = 10;
                        valor = 3.80;
                        break;
                }
                break;
            case 2: //PALENQUE
                switch (positionDestino) {
                    case 0: //STA LUCIA
                        idDestino = 11;
                        valor = 1.80;
                        break;
                    case 1: //PETRILLO
                        idDestino = 12;
                        valor = 1.35;
                        break;
                    case 2: //PALESTINA
                        idDestino = 13;
                        valor = 2.00;
                        break;
                    case 3: //VINCES
                        idDestino = 14;
                        valor = 1.35;
                        break;
                    case 4: //GUAYAQUIL
                        idDestino = 15;
                        valor = 3.80;
                        break;
                }
                break;
            case 3://VINCES
                switch (positionDestino) {
                    case 0: //STA LUCIA
                        idDestino = 16;
                        valor = 1.80;
                        break;
                    case 1: //PETRILLO
                        idDestino = 17;
                        valor = 1.35;
                        break;
                    case 2: //PALESTINA
                        idDestino = 18;
                        valor = 2.00;
                        break;
                    case 3: //PALENQUE
                        idDestino = 19;
                        valor = 1.35;
                        break;
                    case 4: //GUAYAQUIL
                        idDestino = 20;
                        valor = 3.15;
                        break;
                }
                break;
            default:
                idDestino = 0;
                break;
        }
        //valor = valor + anden;
        System.out.println("idDestino: " + idDestino);


    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because january is zero
                final String selectedDate = day + "/" + (month + 1) + "/" + year;
                etPlannedDate.setText(selectedDate);
            }
        });
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    private void listarOrigenes() {
        String[] origenes = {"TERMINAL TERRESTRE DE GUAYAQUIL", "TERMINAL PASCUALES", "TERMINAL PALENQUE", "TERMINAL VINCES"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_list_item_1, origenes);
        spOrigen.setAdapter(adapter);
    }

    //GYE - PASCUALES
    private void listarTTGTP() {
        String[] destino = {"SANTA LUCIA", "PETRILLO", "PALESTINA", "VINCES", "PALENQUE"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_list_item_1, destino);
        spDestino.setAdapter(adapter);
    }

    //PALENQUE
    private void listarTP() {
        String[] destino = {"SANTA LUCIA", "PETRILLO", "PALESTINA", "VINCES", "GUAYAQUIL"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_list_item_1, destino);
        spDestino.setAdapter(adapter);
    }

    //VINCES
    private void listarTV() {
        String[] destino = {"SANTA LUCIA", "PETRILLO", "PALESTINA", "PALENQUE", "GUAYAQUIL"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_list_item_1, destino);
        spDestino.setAdapter(adapter);
    }

}
