package com.lsi.boleteria.fragmentos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lsi.boleteria.R;
import com.lsi.boleteria.activities.LoginActivity;
import com.lsi.boleteria.adapters.BoletosAdapter;
import com.lsi.boleteria.adapters.ListAdapter;
import com.lsi.boleteria.data.entidades.Asiento;
import com.lsi.boleteria.data.entidades.Boleto;
import com.lsi.boleteria.data.entidades.Horario;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.models.BoletoResponse;
import com.lsi.boleteria.data.services.RestUrl;
import com.lsi.boleteria.data.services.Services;
import com.lsi.boleteria.util.Constantes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MisBoletosFragment extends Fragment {

    private ListView lvListBoletos;
    private ArrayList<BoletoResponse> boletoResponses;
    private BoletosAdapter boletosAdapter;
    private Services services;
    private Usuario usuario;

    public MisBoletosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mis_boletos, container, false);

        lvListBoletos = view.findViewById(R.id.lvListBoletos);

        services = new Services();
        usuario = services.getUsuario();
        loadBoletos();

        /*lvListBoletos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                BoletoResponse boleto = boletosAdapter.getItem(position);

                AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                alertDialog.setTitle("Desea Cancelar el Boleto # " + boleto.getNumeroBoleto().toString());
                alertDialog.setMessage("Esta Seguro?");
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                        (dialog, which) -> dialog.dismiss());
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SI",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // sharedPreferencesBoleteria.clearKeys();
                                dialog.dismiss();
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        });
                alertDialog.show();
            }
        });
*/

        return view;
    }

    private void loadBoletos() {

        Constantes.showSimpleProgressDialog(getContext(), "Cargando...", "Boletos", false);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, RestUrl.boletos + usuario.getCliente().getId(),
                response -> {
                    try {
                        JSONArray dataHorarios;
                        JSONObject dataobj, busObj;
                        dataHorarios = new JSONArray(response);
                        boletoResponses = new ArrayList<>();
                        for (int i = 0; i < dataHorarios.length(); i++) {

                            BoletoResponse boletoResponse = new BoletoResponse();
                            dataobj = dataHorarios.getJSONObject(i);
                            boletoResponse.setAnio(dataobj.getInt("anio"));
                            boletoResponse.setNumeroBoleto(dataobj.getLong("numeroBoleto"));
                            boletoResponse.setAsiento(dataobj.getString("asiento"));
                            boletoResponse.setId(dataobj.getInt("id"));
                            boletoResponse.setIdCliente(dataobj.getInt("idCliente"));
                            boletoResponse.setBus(dataobj.getString("bus"));
                            boletoResponse.setUsuarioEmite(dataobj.getString("usuarioEmite"));
                            boletoResponse.setTotal(dataobj.getString("total"));
                            boletoResponse.setFechaSalida(dataobj.getString("fechaSalida"));
                            boletoResponse.setDestino(dataobj.getString("destino"));
                            boletoResponse.setNumeroBoletoFormato(dataobj.getString("numeroBoletoFormato"));
                            boletoResponses.add(boletoResponse);
                        }
                        setupListview();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Toast.makeText(getActivity().getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        requestQueue.add(stringRequest);


    }

    private void setupListview() {
        Constantes.removeSimpleProgressDialog();  //will remove progress dialog
        if (!boletoResponses.isEmpty()) {
            boletosAdapter = new BoletosAdapter(getActivity().getApplicationContext(), boletoResponses);
            lvListBoletos.setAdapter(boletosAdapter);
        }
    }


}
