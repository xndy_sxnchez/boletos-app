package com.lsi.boleteria.data.rest.paymentez;

import com.lsi.boleteria.data.rest.paymentez.model.CreateChargeRequest;
import com.lsi.boleteria.data.rest.paymentez.model.DeleteCardRequest;
import com.lsi.boleteria.data.rest.paymentez.model.DeleteCardResponse;
import com.lsi.boleteria.data.rest.paymentez.model.GetCardsResponse;
import com.lsi.boleteria.data.rest.paymentez.model.VerifyRequest;
import com.lsi.boleteria.data.rest.paymentez.model.VerifyResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface BackendService {

    @GET("paymentez/get-cards")
    Call<GetCardsResponse> getCards(@Query("uid") String uid);

    @GET("/v2/card/list")
    Call<GetCardsResponse> getCardsV2(@Query("uid") String uid);

    @FormUrlEncoded
    @POST("paymentez/delete-card")
    Call<DeleteCardResponse> deleteCard(@Field("uid") String uid, @Field("token") String token);

    @POST("/v2/card/delete/")
    Call<DeleteCardResponse> deleteCardV2(@Body DeleteCardRequest var1);

    @FormUrlEncoded
    @POST("paymentez/create-charge")
    Call<CreateChargeResponse> createCharge(@Field("uid") String uid, @Field("email") String email,  @Field("session_id") String session_id,
                                            @Field("token") String token, @Field("amount") double amount,
                                            @Field("dev_reference") String dev_reference, @Field("description") String description);

    @POST("/v2/transaction/debit/")
    Call<CreateChargeResponse> createChargeV2(@Body CreateChargeRequest var1);

    @FormUrlEncoded
    @POST("paymentez/verify-transaction")
    Call<VerifyResponse> verifyTransaction(@Field("uid") String uid, @Field("transaction_id") String transaction_id,
                                           @Field("type") String type, @Field("value") String value);

    @POST("/v2/transaction/verify")
    Call<VerifyResponse> verifyTransactionV2(@Body VerifyRequest var1);

}
