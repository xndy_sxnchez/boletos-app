package com.lsi.boleteria.adapters;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lsi.boleteria.activities.CompraBoletoActivity;
import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Horario;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Horario> horariosList;

    public ListAdapter(Context context, ArrayList<Horario> horariosList) {

        this.context = context;
        this.horariosList = horariosList;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {
        if (horariosList != null)
            return horariosList.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        return horariosList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.card_horarios, null, true);

            holder.imgUrlBus = convertView.findViewById(R.id.imgUrlBus);
            holder.txtFecha = convertView.findViewById(R.id.txtFecha);
            holder.btnComprar = convertView.findViewById(R.id.btnComprar);
            holder.txtBusDescripcion = convertView.findViewById(R.id.txtBusDescripcion);
            convertView.setTag(holder);
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(context).load(horariosList.get(position).getBus().getUrlImagenBus()).into(holder.imgUrlBus);
        holder.salida = horariosList.get(position).getHoraDescripcion();
        holder.txtFecha.setText(holder.salida);
        holder.bus = horariosList.get(position).getBus().getNombre() + " - "  + horariosList.get(position).getDestino();
        holder.txtBusDescripcion.setText("Bus: " + holder.bus);

        holder.btnComprar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(context, CompraBoletoActivity.class);
                        intent.putExtra("HORARIO", horariosList.get(position));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                }
        );


        return convertView;
    }

    private class ViewHolder {

        protected TextView txtFecha, txtBusDescripcion;
        protected ImageView imgUrlBus;
        protected Button btnComprar;
        protected String salida, bus;
    }

}