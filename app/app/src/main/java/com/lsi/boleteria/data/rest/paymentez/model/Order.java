package com.lsi.boleteria.data.rest.paymentez.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("dev_reference")
    @Expose
    private String devReference;
    @SerializedName("vat")
    @Expose
    private Double vat = 0.00;

    public Order(){

    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDevReference() {
        return devReference;
    }

    public void setDevReference(String devReference) {
        this.devReference = devReference;
    }

    public Double getVat() {
        return vat;
    }

    public void setVat(Double vat) {
        this.vat = vat;
    }
}
