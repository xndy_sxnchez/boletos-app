package com.lsi.boleteria.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lsi.boleteria.BoleteriaApp;
import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Asiento;
import com.lsi.boleteria.data.entidades.Boleto;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.models.BoletoModel;
import com.lsi.boleteria.data.preferences.SharedPreferencesBoleteria;
import com.lsi.boleteria.data.rest.RetrofitFactory;
import com.lsi.boleteria.data.rest.paymentez.BackendService;
import com.lsi.boleteria.data.rest.paymentez.CreateChargeResponse;
import com.lsi.boleteria.data.rest.paymentez.model.CreateChargeRequest;
import com.lsi.boleteria.data.rest.paymentez.model.Order;
import com.lsi.boleteria.data.services.Services;
import com.lsi.boleteria.util.Constantes;
import com.paymentez.android.Paymentez;
import com.paymentez.android.model.Card;
import com.paymentez.android.rest.model.User;

import java.io.IOException;

import retrofit2.Call;

public class PagoBoletoActivity extends AppCompatActivity {

    private Asiento pyzTransaction;

    LinearLayout buttonSelectPayment;
    ImageView imageViewCCImage;
    TextView textViewCCLastFour, textViewValor;
    Button buttonPlaceOrder;
    String CARD_TOKEN = "", CARD_LAST4, CARD_TYPE, USER_ID;
    int SELECT_CARD_REQUEST = 1004;

    ProgressDialog progressDialog;

    double ORDER_AMOUNT = 10.00;

    String ORDER_DESCRIPTION;

    String DEV_REFERENCE;

    private BackendService backendService;

    private Intent intent;
    private Toolbar toolBarPagoBoleto;
    private TextView txtToolbarPagoBoleto;
    private Services services;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pago_boleto);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        Paymentez.setEnvironment(Constantes.PAYMENTEZ_IS_TEST_MODE, Constantes.PAYMENTEZ_CLIENT_APP_CODE, Constantes.PAYMENTEZ_CLIENT_APP_KEY);
        intent = getIntent();

        services = new Services();
        usuario = services.getUsuario();

        toolBarPagoBoleto = findViewById(R.id.toolBarPagoBoleto);
        txtToolbarPagoBoleto = findViewById(R.id.txtToolbarPagoBoleto);
        textViewValor = findViewById(R.id.textViewValor);

        setSupportActionBar(toolBarPagoBoleto);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pyzTransaction = (Asiento) intent.getSerializableExtra("ASIENTO");
        textViewValor.setText("$ " + pyzTransaction.getValor().toString());
        txtToolbarPagoBoleto.setText("Pago de Boleto: #" + pyzTransaction.getNumeroAsiento().toString());
        ORDER_AMOUNT = pyzTransaction.getValor();
        progressDialog = new ProgressDialog(this);
        backendService = RetrofitFactory.getClient().create(BackendService.class);
        //backendService = RetrofitFactory.getClientPz().create(BackendService.class);

        imageViewCCImage = findViewById(R.id.imageViewCCImage);
        textViewCCLastFour = findViewById(R.id.textViewCCLastFour);

        buttonPlaceOrder = findViewById(R.id.buttonPlaceOrder);
        buttonPlaceOrder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CARD_TOKEN == null || CARD_TOKEN.equals("")) {
                    Toast.makeText(PagoBoletoActivity.this, "Se necesita ingresar una tarjeta.", Toast.LENGTH_SHORT).show();
                } else {

                    new AsyncTask<Void, Void, Boleto>() {
                        @Override
                        protected Boleto doInBackground(Void... params) {
                            if (pyzTransaction.getNumeroAsiento() != null)
                                ORDER_DESCRIPTION = pyzTransaction.getNumeroAsiento().toString();
                            if (pyzTransaction.getUbicacion() != null)
                                DEV_REFERENCE = pyzTransaction.getId().toString();
                            if (DEV_REFERENCE == null || ORDER_DESCRIPTION == null)
                                return null;
                            return processPago();
                        }

                        @Override
                        protected void onPostExecute(Boleto createChargeResponse) {
                            super.onPostExecute(createChargeResponse);
                            progressDialog.dismiss();
                            if (createChargeResponse != null && createChargeResponse.getId() != null) {
//                                alertDialog.setTitle("Transacciòn Exitosa Boleto # " + createChargeResponse.getAsiento());
                                alertDialog.setMessage("Transacciòn Exitosa Boleto # " + createChargeResponse.getNumeroBoleto()
                                        + " \n " + usuario.getCliente().getNombre());
                                /*alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                                        (dialog, which) -> dialog.dismiss());*/
                                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                        (dialog, which) -> {
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        });
                                alertDialog.show();
                            }
                        }

                        @Override
                        protected void onProgressUpdate(Void... values) {
                            super.onProgressUpdate(values);
                        }

                        @Override
                        protected void onPreExecute() {
                            progressDialog.setMax(100);
                            progressDialog.setMessage("Procesando Pago ..");
                            progressDialog.setCancelable(false);
                            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressDialog.show();
                            // }
                        }
                    }.execute();
                }
            }
        });

        buttonSelectPayment = findViewById(R.id.buttonSelectPayment);
        buttonSelectPayment.setOnClickListener(v -> {
            Intent intent = new Intent(this, ListCardsActivity.class);
            startActivityForResult(intent, SELECT_CARD_REQUEST);
        });

    }

    private Boleto processPago() {
        BoletoModel boletoModel = new BoletoModel();
        boletoModel.setIdCliente(usuario.getCliente().getId());
        boletoModel.setIdHorario(pyzTransaction.getIdHorario());
        boletoModel.setIdAsiento(pyzTransaction.getId().intValue());
        boletoModel.setValor(pyzTransaction.getValor().toString());

        Boleto b = services.guardarBoleto(boletoModel);

        CreateChargeResponse createChargeResponse = null;

        User user = new User();
        user.setId(usuario.getId().toString());
        user.setEmail(usuario.getCliente().getCorreo());

        Card card = new Card();
        card.setToken(CARD_TOKEN);

        Order order = new Order();
        order.setAmount(ORDER_AMOUNT);
        order.setDescription(b.getNumeroBoleto().toString());
        order.setDevReference(DEV_REFERENCE);

        CreateChargeRequest charge = new CreateChargeRequest();
        charge.setCard(card);
        charge.setUser(user);
        charge.setOrder(order);

        Call<CreateChargeResponse> call = backendService.createCharge(usuario.getId().toString(), usuario.getCliente().getCorreo(), Paymentez.getSessionId(getApplicationContext()),
                CARD_TOKEN, ORDER_AMOUNT, DEV_REFERENCE, b.getNumeroBoleto().toString());

        //Call<CreateChargeResponse> call = backendService.createChargeV2(charge);
        try {
            createChargeResponse = call.execute().body();
            //return createChargeResponse;
        } catch (IOException e) {
            e.printStackTrace();
            //return createChargeResponse;
        }
        return b;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == SELECT_CARD_REQUEST) {
            // Make sure the request was successful
            if (resultCode == this.RESULT_OK) {
                CARD_TOKEN = data.getStringExtra("CARD_TOKEN");
                CARD_TYPE = data.getStringExtra("CARD_TYPE");
                CARD_LAST4 = data.getStringExtra("CARD_LAST4");
                USER_ID = data.getStringExtra("USER_ID");

                if (CARD_LAST4 != null && !CARD_LAST4.equals("")) {
                    textViewCCLastFour.setText("XXXX." + CARD_LAST4);
                    imageViewCCImage.setImageResource(Card.getDrawableBrand(CARD_TYPE));
                }
            }
        }
    }


}
