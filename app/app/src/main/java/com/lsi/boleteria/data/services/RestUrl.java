package com.lsi.boleteria.data.services;

public class RestUrl {

    //public static final String ip = "http://54.172.49.110:8780/";
    //public static final String ip = "http://192.168.100.135:8780/";
    public static final String ip = "http://192.168.1.5:8780/";
    //public static final String ip = "http://192.168.43.13:8780/";

    public static final String urlBase = ip + "boleteria/api/";
    public static final String iniciarSesion = urlBase + "usuario/iniciarSesion";
    public static final String existeUsuario = urlBase + "usuario/nombreUsuario/";
    public static final String crearUsuario = urlBase + "usuario/registro";
    public static final String recuperarUsuario = urlBase + "usuario/idUsuario/";
    public static final String cambiarContrasenia = urlBase + "usuario/idUsuario/";


    public static final String horarios = urlBase + "horario/horarios/";
    public static final String paymentezUrl = ip;

    public static final String guardarBoleto = urlBase + "/boleto/guardarBoleto";
    public static final String boletos = urlBase + "/boleto/boletos/";
    public static final String numBoleto = urlBase + "/boleto/numBoleto/";


}
