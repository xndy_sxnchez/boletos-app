package com.lsi.boleteria.fragmentos;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.models.BoletoResponse;
import com.lsi.boleteria.data.services.Services;


public class BuscarBoletoFragment extends Fragment implements View.OnClickListener {

    private BottomNavigationView bottomNavigationView;
    private ProgressBar progressBar;
    private Boolean goodByeBottom;
    private CardView cardViewBoleto;
    private LinearLayout layoutBusqueda, layoutInfoConsultaBoleto, layourNotFoundBoleto;
    private Services services;
    private Usuario usuario;
    private Toolbar toolBarBuscarBoleto;
    protected TextView txtFecha, txtBusDescripcion, txtDestino, txtAsiento, txtValor, txtPropietario;

    public BuscarBoletoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        goodByeBottom = Boolean.FALSE;
        View view = inflater.inflate(R.layout.fragment_buscar_boleto, container, false);
        bottomNavigationView = getActivity().findViewById(R.id.nav_view);

        layoutBusqueda = view.findViewById(R.id.layoutBusqueda);
        layoutInfoConsultaBoleto = view.findViewById(R.id.layoutInfoConsultaBoleto);
        layourNotFoundBoleto = view.findViewById(R.id.layourNotFoundBoleto);
        cardViewBoleto = view.findViewById(R.id.cardViewBoleto);
        cardViewBoleto.setOnClickListener(this);
        services = new Services();

        toolBarBuscarBoleto = view.findViewById(R.id.toolBarBuscarBoleto);
        toolBarBuscarBoleto.setTitle("");

        txtDestino = view.findViewById(R.id.txtDestino);
        txtFecha = view.findViewById(R.id.txtFecha);
        txtAsiento = view.findViewById(R.id.txtAsiento);
        txtValor = view.findViewById(R.id.txtValor);
        txtBusDescripcion = view.findViewById(R.id.txtBusDescripcion);
        txtPropietario = view.findViewById(R.id.txtPropietario);



        progressBar = view.findViewById(R.id.boleto_progress_bar);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolBarBuscarBoleto);

        return view;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_boletos, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);


        searchViewItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                goodByeBottom = Boolean.TRUE;
                bottomNavigationView.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                goodByeBottom = Boolean.FALSE;
                bottomNavigationView.setVisibility(View.VISIBLE);
                return true;
            }
        });

        SearchView searchView = (SearchView) searchViewItem.getActionView();
        if (searchView != null) {
            searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("onQueryTextSubmit", query);
                new AsyncTask<Void, Void, BoletoResponse>() {
                    @Override
                    protected BoletoResponse doInBackground(Void... params) {
                        return services.findBoleto(Long.valueOf(query));
                    }

                    @Override
                    protected void onPostExecute(BoletoResponse result) {
                        super.onPostExecute(result);
                        hideProgressBar();
                        if (result != null) {
                            if (result.getId() == null) enableComponent(true, false);
                            else {
                                //mViewModel.getDatosProformaMutableLiveData().setValue(result);
                                enableComponent(true, true);
                                txtDestino.setText("Destino: " + result.getDestino());
                                //txtDestino.setTextColor(Color.GREEN);
                                txtFecha.setText("Fecha: " + " " + result.getFechaSalida());
                                txtAsiento.setText("Asiento:" + " " + result.getAsiento());
                                txtValor.setText("Valor: $" + " " + result.getTotal());
                                txtBusDescripcion.setText("Boleto: " +  result.getNumeroBoleto().toString() +
                                        " - Anden 48 -SP1 - Bus: " + result.getBus());
                                txtBusDescripcion.setTextColor(Color.RED);
                                txtPropietario.setText("Pasajero: " + result.getUsuarioEmite());
                                searchView.clearFocus();
                            }
                        } else enableComponent(true, false);
                    }

                    @Override
                    protected void onPreExecute() {
                        showProgressBar();
                    }
                }.execute();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //    Log.i("onQueryTextChange", newText);
                enableComponent(false, false);
                return true;
            }
        });


    }

    public void showProgressBar() {
        layoutInfoConsultaBoleto.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.animate().setDuration(200).alpha(1).start();

    }


    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        progressBar.animate().setDuration(200).alpha(0).start();
    }


    private void enableComponent(Boolean isSubmit, Boolean hasData) {
        if (isSubmit) {
            if (hasData) {
                layoutInfoConsultaBoleto.setVisibility(View.GONE);
                layourNotFoundBoleto.setVisibility(View.GONE);
                layoutBusqueda.setVisibility(View.VISIBLE);
                bottomNavigationView.setVisibility(View.GONE);


            } else {
                layoutInfoConsultaBoleto.setVisibility(View.GONE);
                layoutBusqueda.setVisibility(View.GONE);
                layourNotFoundBoleto.setVisibility(View.VISIBLE);
            }
        } else {
            layoutInfoConsultaBoleto.setVisibility(View.VISIBLE);
            layoutBusqueda.setVisibility(View.GONE);
            layourNotFoundBoleto.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResume() {
        super.onResume();
        //Log.i(TAG, "onResume");
        if (bottomNavigationView != null && goodByeBottom)
            bottomNavigationView.setVisibility(View.GONE);
    }
}
