package com.lsi.boleteria.data.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.lsi.boleteria.data.dao.BoletoDao;
import com.lsi.boleteria.data.dao.ClienteDao;
import com.lsi.boleteria.data.dao.UsuarioDao;
import com.lsi.boleteria.data.entidades.Boleto;
import com.lsi.boleteria.data.entidades.Cliente;
import com.lsi.boleteria.data.entidades.Usuario;

@Database(entities = {Usuario.class, Boleto.class, Cliente.class}, version = 2, exportSchema = false)
public abstract class BoleteriaDataBase extends RoomDatabase {

    private static BoleteriaDataBase INSTANCE;
    private static final String DATABASE_NAME = "boleteria";

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static volatile BoleteriaDataBase sInstance;

    public static BoleteriaDataBase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null) {
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            BoleteriaDataBase.class, BoleteriaDataBase.DATABASE_NAME)
                            .allowMainThreadQueries()
                            //AGREGA COLUMNAS
                            //.addMigrations(MIGRATION_DB)
                            //CREA NUEVAMENTE LA BASE
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return sInstance;

    }

    public abstract UsuarioDao usuarioDao();
    public abstract ClienteDao clienteDao();
    public abstract BoletoDao boletoDao();

}
