package com.lsi.boleteria.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.services.Services;

public class CambiarContraseniaActivity extends AppCompatActivity {

    private EditText editTextContraseniaCambio, editTextRepetirContraseniaCambio, editTextNombreUsuario;
    private Button btnRegistrarseCambio;
    private Services services;
    private Usuario usuario;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_contrasenia);

        Toolbar toolbar = findViewById(R.id.toolbarConfirmacionCambio);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        services = new Services();
        usuario = services.getUsuario();
        progressDialog = new ProgressDialog(this);
        editTextContraseniaCambio = findViewById(R.id.editTextContraseniaCambio);
        editTextRepetirContraseniaCambio = findViewById(R.id.editTextRepetirContraseniaCambio);
        editTextNombreUsuario = findViewById(R.id.editTextNombreUsuario);
        editTextNombreUsuario.setText(usuario.getNombreUsuario());
        btnRegistrarseCambio = findViewById(R.id.btnRegistrarseCambio);

        btnRegistrarseCambio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextContraseniaCambio.getText().toString().length() >4){
                    if(editTextContraseniaCambio.getText().toString().equals(editTextRepetirContraseniaCambio.getText().toString())){
                        String contrasenia = editTextContraseniaCambio.getText().toString();
                        new AsyncTask<Void, Void, Boolean>() {
                            @Override
                            protected Boolean doInBackground(Void... params) {
                                try {
                                    services.cambiarContrasenia(usuario.getId().longValue(), contrasenia);
                                    return true;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return false;
                            }

                            @Override
                            protected void onPostExecute(Boolean result) {
                                progressDialog.dismiss();
                                Toast.makeText(CambiarContraseniaActivity.this, "Contraseña Actualizada", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            protected void onProgressUpdate(Void... values) {
                                super.onProgressUpdate(values);
                            }

                            @Override
                            protected void onPreExecute() {
                                progressDialog.setMax(100);
                                progressDialog.setMessage("Validando Usuario ...");
                                progressDialog.setCancelable(false);
                                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressDialog.show();
                                super.onPreExecute();
                            }
                        }.execute();

                    }else{
                        Toast.makeText(CambiarContraseniaActivity.this, "Las contraseñas deben Coincidir", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(CambiarContraseniaActivity.this, "Las contraseñas deben tener mas de 4 digitos", Toast.LENGTH_SHORT).show();
                }

            }
        });



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
