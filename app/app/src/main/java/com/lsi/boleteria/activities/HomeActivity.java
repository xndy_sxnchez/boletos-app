package com.lsi.boleteria.activities;

import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.lsi.boleteria.BoleteriaApp;
import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.preferences.SharedPreferencesBoleteria;
import com.lsi.boleteria.util.Constantes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    private NavController navController;
    private BottomNavigationView navView;

    private SharedPreferencesBoleteria sharedPreferencesBoleteria;
    private Usuario usuario;
    private Gson gson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferencesBoleteria = SharedPreferencesBoleteria.getInstance(BoleteriaApp.getAppContext());
        gson = new Gson();
        usuario = gson.fromJson(sharedPreferencesBoleteria.getKey(Constantes.USER), Usuario.class);
        setContentView(R.layout.activity_home);
        navView = findViewById(R.id.nav_view);
        navController = Navigation.findNavController(this, R.id.nav_host_boleteria);

        NavigationUI.setupWithNavController(navView, navController);

        Menu menu = navView.getMenu();

        MenuItem menuItem = menu.findItem(R.id.navigation_buscar);
        if (usuario != null) {
            if (usuario.getEsAdmin() != null)
                menuItem.setVisible(usuario.getEsAdmin());
            else{
                menuItem.setVisible(true);
            }
        }
    }

}
