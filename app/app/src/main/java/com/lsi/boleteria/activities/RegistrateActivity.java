package com.lsi.boleteria.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Cliente;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.models.UsuarioModel;
import com.lsi.boleteria.data.services.Services;
import com.lsi.boleteria.util.Constantes;

public class RegistrateActivity extends AppCompatActivity implements View.OnClickListener {

    private Services services;

    private Button btnRegistrarse;
    private ImageView imgButonSearchCedula;
    private EditText editTextNombreUsuario, editTextContrasenia, editTextRepetirContrasenia;

    private String validado = "NO";

    private ProgressDialog progressDialog;

    private Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrate);
        Constantes.hideKeyboardV2(this);

        cliente = (Cliente) getIntent().getSerializableExtra("cliente");


        btnRegistrarse = findViewById(R.id.btnRegistrarse);
        btnRegistrarse.setOnClickListener(this);

        imgButonSearchCedula = findViewById(R.id.imgButonSearchCedula);
        imgButonSearchCedula.setOnClickListener(this);

        editTextNombreUsuario = findViewById(R.id.editTextNombreUsuario);
        editTextContrasenia = findViewById(R.id.editTextContrasenia);
        editTextRepetirContrasenia = findViewById(R.id.editTextRepetirContrasenia);


        Toolbar toolbar = findViewById(R.id.toolbarConfirmacion);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressDialog = new ProgressDialog(this);

        services = new Services();

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgButonSearchCedula:
                if (validarDatos()) {
                    String nombreUsuario = editTextNombreUsuario.getText().toString();
                    new AsyncTask<Void, Void, Boolean>() {
                        @Override
                        protected Boolean doInBackground(Void... params) {
                            try {
                                UsuarioModel usuarioModel = services.existeUsuario(nombreUsuario);
                                if (usuarioModel == null) {
                                    return true;
                                }
                                if (usuarioModel.getIdUsuario() == null) {
                                    return true;
                                }
                                return false;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return false;
                        }

                        @Override
                        protected void onPostExecute(Boolean result) {
                            progressDialog.dismiss();
                            if (result) {
                                validado = "SI";
                                disponible();
                            } else {
                                validado = "NO";
                                noDisponible();
                            }
                        }

                        @Override
                        protected void onProgressUpdate(Void... values) {
                            super.onProgressUpdate(values);
                        }

                        @Override
                        protected void onPreExecute() {
                            progressDialog.setMax(100);
                            progressDialog.setMessage("Validando Usuario ...");
                            // progressDialog.setTitle(R.string.iniciando_sesion);
                            progressDialog.setCancelable(false);
                            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressDialog.show();
                            super.onPreExecute();
                        }
                    }.execute();
                }
                break;
            case R.id.btnRegistrarse:
                if (validado.equals("SI")) {
                    if (validarContrasenia()) {
                        Usuario usuario = new Usuario(editTextNombreUsuario.getText().toString(),
                                editTextContrasenia.getText().toString(), cliente);
                        new AsyncTask<Void, Void, Boolean>() {
                            @Override
                            protected Boolean doInBackground(Void... params) {
                                try {
                                    return services.crearUsuario(usuario);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return false;
                            }

                            @Override
                            protected void onPostExecute(Boolean result) {
                                progressDialog.dismiss();
                                if (result) {
                                    nextActivity();
                                } else {
                                    validado = "NO";
                                    noDisponible();
                                }
                            }

                            @Override
                            protected void onProgressUpdate(Void... values) {
                                super.onProgressUpdate(values);
                            }

                            @Override
                            protected void onPreExecute() {
                                progressDialog.setMax(100);
                                progressDialog.setMessage("Registrando Usuario ...");
                                // progressDialog.setTitle(R.string.iniciando_sesion);
                                progressDialog.setCancelable(false);
                                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressDialog.show();
                                super.onPreExecute();
                            }
                        }.execute();
                    }
                } else {
                    Toast.makeText(this, "Debe validar el usuario ingresado", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void disponible() {
        Toast.makeText(this, "Usuario se encuentra disponible", Toast.LENGTH_SHORT).show();
    }

    private void noDisponible() {
        Toast.makeText(this, "Usuario ya ha sido registrado anteriormente", Toast.LENGTH_SHORT).show();
    }

    private void nextActivity() {
        startActivity(new Intent(this, HomeActivity.class));
    }

    private Boolean validarDatos() {
        if (editTextNombreUsuario.getText().toString().length() <= 3
                || editTextNombreUsuario.getText().toString().length() > 16) {
            Toast.makeText(this, "Ingrese un Nombre de Usuario entre 3 y 15 caracteres ", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private Boolean validarContrasenia() {
        if (!editTextContrasenia.getText().toString().equals(editTextRepetirContrasenia.getText().toString())) {
            Toast.makeText(this, "Las Contraseñas deben Coincidir", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (editTextContrasenia.getText().toString().length() <= 4) {
            Toast.makeText(this, "Ingrese una Contraseña mayor a 4 caracteres ", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}
