package com.lsi.boleteria.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.lsi.boleteria.data.entidades.Cliente;

@Dao
public interface ClienteDao {

    @Query(value = "SELECT * FROM cliente c WHERE c.id = :idUsuario")
    Cliente findById(Integer idUsuario);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUsuario(Cliente cliente);

    @Query(value = "DELETE FROM cliente")
    void deleteAll();

}
