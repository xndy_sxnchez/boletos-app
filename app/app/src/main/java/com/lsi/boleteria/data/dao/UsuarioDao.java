package com.lsi.boleteria.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.lsi.boleteria.data.entidades.Usuario;

@Dao
public interface UsuarioDao {

    @Query(value = "SELECT * FROM usuario u WHERE u.nombreUsuario = :nombreUsuario")
    Usuario findByNombreUsuario(String nombreUsuario);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUsuario(Usuario usuario);

    @Query(value = "DELETE FROM usuario")
    void deleteAll();


}
