package com.lsi.boleteria.data.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesBoleteria {


    private static SharedPreferencesBoleteria sharedPreferencesBoleteria;
    private SharedPreferences sharedPreferences;

    public static SharedPreferencesBoleteria getInstance(Context context) {
        if (sharedPreferencesBoleteria == null) {
            sharedPreferencesBoleteria = new SharedPreferencesBoleteria(context);
        }
        return sharedPreferencesBoleteria;
    }

    public SharedPreferencesBoleteria(Context context) {
        sharedPreferences = context.getSharedPreferences("SharedPreferencesBoleteria", Context.MODE_PRIVATE);
    }

    public void clearKeys() {
        sharedPreferences.edit().clear().commit();
    }



    public void saveKey(String key, String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public String getKey(String key) {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, "");
        }
        return "";
    }


}
