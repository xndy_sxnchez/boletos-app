package com.lsi.boleteria.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Cliente;
import com.lsi.boleteria.util.Constantes;

public class DatosRegistroActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnSiguienteConfirmacion;
    private EditText editTextNumeroCedula, editTextNombres,
            editTextApellidos,  editTextDireccion,
            editTextTelefono, editTextCorreo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_registro);
        Constantes.hideKeyboardV2(this);

        editTextNumeroCedula = findViewById(R.id.editTextNumeroCedula);
        editTextNombres = findViewById(R.id.editTextNombres);
        editTextApellidos = findViewById(R.id.editTextApellidos);
        editTextDireccion = findViewById(R.id.editTextDireccion);
        //editTextFechaInscripcion = findViewById(R.id.editTextFechaInscripcion);
        editTextTelefono = findViewById(R.id.editTextTelefono);
        editTextCorreo = findViewById(R.id.editTextCorreo);

        btnSiguienteConfirmacion = findViewById(R.id.btnSiguienteConfirmacion);
        btnSiguienteConfirmacion.setOnClickListener(this);


        Toolbar toolbar = findViewById(R.id.toolbarRegistro);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //   toolbar.setNavigationOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnSiguienteConfirmacion:
                if (validarDatos()) {
                    Cliente cliente = new Cliente(editTextNumeroCedula.getText().toString(), editTextNombres.getText().toString(),
                            editTextApellidos.getText().toString(), editTextDireccion.getText().toString(),
                            editTextCorreo.getText().toString(), editTextTelefono.getText().toString());
                    System.out.println("cliente  " + cliente.toString());
                    Intent mainIntent = new Intent(this, RegistrateActivity.class);
                    mainIntent.putExtra("cliente", cliente);
                    this.startActivity(mainIntent);

                }
                break;
        }
    }

    private Boolean validarDatos() {
        if (editTextNumeroCedula.getText().toString() != null && editTextNumeroCedula.getText().toString().length() < 10) {
            Toast.makeText(this, "Ingrese un número de Cédula Correcto", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (editTextNombres.getText().toString() != null && editTextNombres.getText().toString().length() < 1) {
            Toast.makeText(this, "Ingrese sus nombres", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (editTextApellidos.getText().toString() != null && editTextApellidos.getText().toString().length() < 1) {
            Toast.makeText(this, "Ingrese sus apellidos", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (editTextDireccion.getText().toString() != null && editTextDireccion.getText().toString().length() < 1) {
            Toast.makeText(this, "Ingrese su dirección", Toast.LENGTH_SHORT).show();
            return false;
        }
        /*if (editTextFechaInscripcion.getText().toString() != null && editTextFechaInscripcion.getText().toString().length() < 1) {
            Toast.makeText(this, "Ingrese su Fecha de Nacimiento", Toast.LENGTH_SHORT).show();
            return false;
        }*/
        if (editTextTelefono.getText().toString() != null && editTextTelefono.getText().toString().length() < 10) {
            Toast.makeText(this, "Ingrese un Teléfono correcto", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (editTextCorreo.getText().toString() != null && editTextCorreo.getText().toString().length() < 5) {
            Toast.makeText(this, "Ingrese su correo electrònico", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


}
