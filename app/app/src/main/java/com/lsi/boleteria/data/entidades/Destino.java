/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.data.entidades;

import java.io.Serializable;
import java.util.List;

public class Destino implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String nombre;
    private String descripcion;
    private List<Horario> horarioList;

    public Destino() {
    }

    public Destino(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Horario> getHorarioList() {
        return horarioList;
    }

    public void setHorarioList(List<Horario> horarioList) {
        this.horarioList = horarioList;
    }

    
    @Override
    public String toString() {
        return "com.lsi.boleteria.data.entidades.Destino[ id=" + id + " ]";
    }

}
