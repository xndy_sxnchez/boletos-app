/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.data.entidades;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(tableName = "usuario")
public class Usuario implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Integer id;
    private String nombreUsuario;
    private String contrasena;
    private Integer idCliente;
    private Boolean estado;
    private Boolean esAdmin;
    @Ignore
    private Cliente cliente;

    @Ignore
    public Usuario() {
    }

    @Ignore
    public Usuario(String nombreUsuario, String contrasena, Cliente cliente) {
        this.nombreUsuario = nombreUsuario;
        this.contrasena = contrasena;
        this.cliente = cliente;
    }
    @Ignore
    public Usuario(String nombreUsuario, String contrasena) {
        this.nombreUsuario = nombreUsuario;
        this.contrasena = contrasena;
        this.estado = Boolean.TRUE;
    }

    public Usuario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Boolean getEsAdmin() {
        return esAdmin;
    }

    public void setEsAdmin(Boolean esAdmin) {
        this.esAdmin = esAdmin;
    }

    @Override
    public String toString() {
        return "com.lsi.boleteria.data.entidades.Usuario[ id=" + id + " ]";
    }
    
}
