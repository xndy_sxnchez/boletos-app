package com.lsi.boleteria.fragmentos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lsi.boleteria.BoleteriaApp;
import com.lsi.boleteria.R;
import com.lsi.boleteria.activities.CambiarContraseniaActivity;
import com.lsi.boleteria.activities.DatosRegistroActivity;
import com.lsi.boleteria.activities.LoginActivity;
import com.lsi.boleteria.activities.PerfilActivity;
import com.lsi.boleteria.data.preferences.SharedPreferencesBoleteria;


public class ConfiguracionesFragment extends Fragment implements View.OnClickListener {

    private CardView cardCerrarSesion, cardPerfil, cardContra;
    private SharedPreferencesBoleteria sharedPreferencesBoleteria;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_configuraciones, container, false);

        sharedPreferencesBoleteria = SharedPreferencesBoleteria.getInstance(BoleteriaApp.getAppContext());

        cardCerrarSesion = view.findViewById(R.id.cardCerrarSesion);
        cardPerfil = view.findViewById(R.id.cardPerfil);
        cardContra = view.findViewById(R.id.cardContra);

        cardCerrarSesion.setOnClickListener(this);
        cardPerfil.setOnClickListener(this);
        cardContra.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cardPerfil:
                Intent mainIntent = new Intent(getActivity(), PerfilActivity.class);
                getActivity().startActivity(mainIntent);
                break;
            case R.id.cardCerrarSesion:
                AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                alertDialog.setTitle("Cerrar Sesión");
                alertDialog.setMessage("Esta Seguro?");
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                        (dialog, which) -> dialog.dismiss());
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SI",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                sharedPreferencesBoleteria.clearKeys();
                                dialog.dismiss();
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        });
                alertDialog.show();
                break;
            case  R.id.cardContra:
                Intent intent = new Intent(getActivity(), CambiarContraseniaActivity.class);
                getActivity().startActivity(intent);
                break;
        }
    }
}
