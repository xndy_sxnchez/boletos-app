package com.lsi.boleteria.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lsi.boleteria.R;
import com.lsi.boleteria.adapters.TarjetasAdapter;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.rest.RetrofitFactory;
import com.lsi.boleteria.data.rest.paymentez.BackendService;
import com.lsi.boleteria.data.rest.paymentez.model.DeleteCardRequest;
import com.lsi.boleteria.data.rest.paymentez.model.DeleteCardResponse;
import com.lsi.boleteria.data.rest.paymentez.model.GetCardsResponse;
import com.lsi.boleteria.data.services.Services;
import com.lsi.boleteria.util.Constantes;
import com.paymentez.android.model.Card;
import com.paymentez.android.rest.model.ErrorResponse;
import com.paymentez.android.rest.model.User;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListCardsActivity extends AppCompatActivity {

    ArrayList<Card> listCard;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Button buttonAddCard;

    Context mContext;
    BackendService backendService;
    private ProgressDialog progressDialog;
    private TextView tvInfoMisTarjetas;

    private Services services;
    private Usuario usuario;

    private Toolbar toolBarPagoBoleto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_cards);

        services = new Services();
        usuario = services.getUsuario();

        toolBarPagoBoleto = findViewById(R.id.toolBarPagoBoleto);
        setSupportActionBar(toolBarPagoBoleto);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = this;
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        tvInfoMisTarjetas = findViewById(R.id.tvInfoMisTarjetas);
        tvInfoMisTarjetas.setVisibility(View.GONE);
        buttonAddCard = findViewById(R.id.buttonAddCard);
        buttonAddCard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AddCardActivity.class);
                startActivity(intent);
            }
        });

        mRecyclerView = findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        backendService = RetrofitFactory.getClient().create(BackendService.class);
        //backendService = RetrofitFactory.getClientPz().create(BackendService.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getCards();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getCards() {

        progressDialog = new ProgressDialog(ListCardsActivity.this);
        progressDialog.setMessage("Cargando Tarjetas de Crédito");
        progressDialog.show();
        progressDialog.setCancelable(false);
        backendService.getCards(usuario.getId().toString()).enqueue(new Callback<GetCardsResponse>() {
            //backendService.getCardsV2(usuario.getId().toString()).enqueue(new Callback<GetCardsResponse>() {
            //backendService.getCardsV2("90").enqueue(new Callback<GetCardsResponse>() {
            @Override
            public void onResponse(Call<GetCardsResponse> call, Response<GetCardsResponse> response) {
                progressDialog.dismiss();
                GetCardsResponse getCardsResponse = response.body();
                if (response.isSuccessful()) {
                    listCard = (ArrayList<Card>) getCardsResponse.getCards();
                    if (listCard == null) tvInfoMisTarjetas.setVisibility(View.VISIBLE);
                    if (listCard != null && listCard.isEmpty())
                        tvInfoMisTarjetas.setVisibility(View.VISIBLE);
                    mAdapter = new TarjetasAdapter(listCard, new TarjetasAdapter.OnCardSelectedListener() {
                        @Override
                        public void onItemClick(Card card) {
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("CARD_TOKEN", card.getToken());
                            returnIntent.putExtra("CARD_TYPE", card.getType());
                            returnIntent.putExtra("CARD_LAST4", card.getLast4());
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }, new TarjetasAdapter.OnCardDeletedClickListener() {
                        @Override
                        public void onItemClick(Card card) {
                            deleteCard(card);

                        }
                    });

                    mRecyclerView.setAdapter(mAdapter);
                } else {
                    Gson gson = new GsonBuilder().create();
                    try {

                        ErrorResponse errorResponse = gson.fromJson(response.errorBody().string(), ErrorResponse.class);
                        System.out.println(" errorResponse.toString() " + errorResponse.toString());
                        Constantes.alertShow(mContext, "Error " + errorResponse.toString(), errorResponse.getError().getType());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetCardsResponse> call, Throwable e) {
                progressDialog.dismiss();
                Constantes.alertShow(mContext, "Error", e.getLocalizedMessage());
            }
        });
    }


    public void deleteCard(Card card) {

        progressDialog = new ProgressDialog(ListCardsActivity.this);
        progressDialog.setMessage("Eliminando Tarjeta de Crédito");
        progressDialog.show();
        progressDialog.setCancelable(false);

        User user = new User();
        user.setId(usuario.getId().toString());
        DeleteCardRequest del = new DeleteCardRequest();
        del.setCard(card);
        del.setUser(user);

//        backendService.deleteCardV2(del).enqueue(new Callback<DeleteCardResponse>() {
        backendService.deleteCard(usuario.getId().toString(), card.getToken()).enqueue(new Callback<DeleteCardResponse>() {
            @Override
            public void onResponse(Call<DeleteCardResponse> call, Response<DeleteCardResponse> response) {
                progressDialog.dismiss();
                DeleteCardResponse deleteCardResponse = response.body();
                if (response.isSuccessful()) {
                    getCards();
                    Constantes.alertShow(mContext, deleteCardResponse.getMessage(), "Tarjeta de Crédito eliminada Correctamente");
                } else {
                    Gson gson = new GsonBuilder().create();
                    try {
                        ErrorResponse errorResponse = gson.fromJson(response.errorBody().string(), ErrorResponse.class);
                        Constantes.alertShow(mContext, "Error: ", "No se pudo eliminar la tarjeta.");
                        Log.i("ERROR-TYPE: ", errorResponse.getError().getType());
                        Log.i("ERROR-HELP: ", errorResponse.getError().getHelp());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<DeleteCardResponse> call, Throwable e) {
                progressDialog.dismiss();
                Constantes.alertShow(mContext, "Error: ", "No se pudo eliminar la tarjeta...");
            }
        });
    }


}
