package com.lsi.boleteria.data.rest.paymentez.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.paymentez.android.model.Card;
import com.paymentez.android.rest.model.User;

public class DeleteCardRequest {

    @SerializedName("card")
    @Expose
    private Card card;
    @SerializedName("user")
    @Expose
    private User user;

    public DeleteCardRequest() {
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
