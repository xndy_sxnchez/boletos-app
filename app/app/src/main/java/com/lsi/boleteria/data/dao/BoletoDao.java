package com.lsi.boleteria.data.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.lsi.boleteria.data.entidades.Boleto;

import java.util.List;

@Dao
public interface BoletoDao {

    @Query(value = "SELECT * FROM boleto")
    List<Boleto> findAll();

}
