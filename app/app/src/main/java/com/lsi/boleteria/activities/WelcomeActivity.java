package com.lsi.boleteria.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.lsi.boleteria.BoleteriaApp;
import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.preferences.SharedPreferencesBoleteria;
import com.lsi.boleteria.util.Constantes;

public class WelcomeActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 2000;

    private SharedPreferencesBoleteria sharedPreferencesBoleteria;
    private Intent mainIntent;
    private Usuario usuario;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        // getActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_welcome);
        sharedPreferencesBoleteria = SharedPreferencesBoleteria.getInstance(BoleteriaApp.getAppContext());
        gson = new Gson();
        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(() -> {
            /* Create an Intent that will start the Menu-Activity. */

            if (sharedPreferencesBoleteria.getKey(Constantes.USER) != null) {
                usuario = gson.fromJson(sharedPreferencesBoleteria.getKey(Constantes.USER), Usuario.class);
                if (usuario != null && usuario.getId() != null)
                    mainIntent = new Intent(WelcomeActivity.this, HomeActivity.class);
                else
                    mainIntent = new Intent(WelcomeActivity.this, LoginActivity.class);
            } else {
                mainIntent = new Intent(WelcomeActivity.this, LoginActivity.class);
            }
            WelcomeActivity.this.startActivity(mainIntent);
            finish();


        }, SPLASH_DISPLAY_LENGTH);
    }
}
