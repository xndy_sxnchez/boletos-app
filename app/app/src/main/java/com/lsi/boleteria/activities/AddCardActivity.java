package com.lsi.boleteria.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.services.Services;
import com.lsi.boleteria.util.Constantes;
import com.paymentez.android.Paymentez;
import com.paymentez.android.model.Card;
import com.paymentez.android.rest.TokenCallback;
import com.paymentez.android.rest.model.PaymentezError;
import com.paymentez.android.view.CardMultilineWidget;

public class AddCardActivity extends AppCompatActivity implements View.OnClickListener {


    Button buttonNext;
    CardMultilineWidget cardWidget;
    Context mContext;
   // private AclUser user;
    private ProgressDialog progressDialog;
    private Toolbar toolBarAddCardPagoBoleto;

    private Services services;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        mContext = this;
        services = new Services();
        usuario = services.getUsuario();
        cardWidget = findViewById(R.id.card_multiline_widget);
        buttonNext = findViewById(R.id.buttonAddCard);
        buttonNext.setOnClickListener(this);


        toolBarAddCardPagoBoleto = findViewById(R.id.toolBarAddCardPagoBoleto);
        setSupportActionBar(toolBarAddCardPagoBoleto);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        buttonNext.setEnabled(false);

        Card cardToSave = cardWidget.getCard();
        if (cardToSave == null) {
            buttonNext.setEnabled(true);
            Constantes.alertShow(mContext, "Error: ", "Tarjeta Inválida");
            return;
        } else {

            progressDialog = new ProgressDialog(AddCardActivity.this);
            progressDialog.setMessage("Cargando Tarjeta de Crédito");
            progressDialog.show();
            progressDialog.setCancelable(false);

            Paymentez.addCard(this, usuario.getId().toString(), usuario.getCliente().getCorreo(),
                    cardToSave, new TokenCallback() {
            //Paymentez.addCard(getApplicationContext(), "90", "gm", cardToSave, new TokenCallback() {
                public void onSuccess(Card card) {
                    buttonNext.setEnabled(true);
                    progressDialog.dismiss();
                    if (card != null) {
                        if (card.getStatus().equals("valid")) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(AddCardActivity.this);
                            builder1.setMessage("Tarjeta de Crédito Agregada Correctamente");
                            builder1.setCancelable(false);
                            builder1.setPositiveButton(
                                    "OK",
                                    (dialog, id) -> finish());

                            AlertDialog alert11 = builder1.create();
                            alert11.show();


                        } else if (card.getStatus().equals("review")) {
                            Constantes.alertShow(mContext, "Tarjeta en Revisión", null);
                        } else {
                            Constantes.alertShow(mContext, "Error con Tarjeta:", card.getMessage());
                        }
                    }
                }

                public void onError(PaymentezError error) {
                    System.out.println("description: "  + error.getDescription()
                            + " help: " + error.getHelp() +
                            " type: " + error.getType()
                            + " error: " + error.toString());
                    buttonNext.setEnabled(true);
                    progressDialog.dismiss();
                    Constantes.alertShow(mContext, "Error", null);
                }
            });

        }
    }
}
