package com.lsi.boleteria.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Cliente;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.services.Services;

public class PerfilActivity extends AppCompatActivity {

    private EditText editTextNumeroCedula, editTextNombres,
            editTextApellidos, editTextDireccion,
            editTextTelefono, editTextCorreo;

    private Button btnGuardarDatosUsuario;
    private ProgressDialog progressDialog;
    private Services services;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        btnGuardarDatosUsuario = findViewById(R.id.btnGuardarDatosUsuario);

        //editTextNumeroCedula = findViewById(R.id.editTextNumeroCedula);
        editTextNombres = findViewById(R.id.editTextNombres);
        editTextApellidos = findViewById(R.id.editTextApellidos);
        editTextDireccion = findViewById(R.id.editTextDireccion);
        editTextTelefono = findViewById(R.id.editTextTelefono);
        editTextCorreo = findViewById(R.id.editTextCorreo);

        Toolbar toolbar = findViewById(R.id.toolbarRegistro);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        services = new Services();
        usuario = services.getUsuario();
        progressDialog = new ProgressDialog(this);
        ///editTextNumeroCedula.setText(usuario.getCliente().getCedula());
        editTextNombres.setText(usuario.getCliente().getNombre());
        editTextApellidos.setText(usuario.getCliente().getApellido());
        editTextDireccion.setText(usuario.getCliente().getDireccion());
        editTextTelefono.setText(usuario.getCliente().getTelefono());
        editTextCorreo.setText(usuario.getCliente().getCorreo());

        btnGuardarDatosUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editTextNombres.getText().length() > 0 && editTextApellidos.getText().length() > 0
                        && editTextDireccion.getText().length() > 0 && editTextCorreo.getText().length() > 0 &&
                        editTextTelefono.getText().length() > 0) {

                } else {
                    Toast.makeText(PerfilActivity.this, "Debe Ingresar todos los campos", Toast.LENGTH_SHORT).show();
                }

                Cliente cliente = new Cliente(editTextNombres.getText().toString(),
                        editTextApellidos.getText().toString(), editTextDireccion.getText().toString(),
                        editTextCorreo.getText().toString(), editTextTelefono.getText().toString());

                usuario.setCliente(cliente);

                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... params) {
                        try {
                            return services.crearUsuario(usuario);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        progressDialog.dismiss();
                        if (result) {
                            Toast.makeText(PerfilActivity.this, "Datos Actualizados", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(PerfilActivity.this, "Intente Nuevamente", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    protected void onProgressUpdate(Void... values) {
                        super.onProgressUpdate(values);
                    }

                    @Override
                    protected void onPreExecute() {
                        progressDialog.setMax(100);
                        progressDialog.setMessage("Actualizando Usuario ...");
                        // progressDialog.setTitle(R.string.iniciando_sesion);
                        progressDialog.setCancelable(false);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();
                        super.onPreExecute();
                    }
                }.execute();


            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



}
