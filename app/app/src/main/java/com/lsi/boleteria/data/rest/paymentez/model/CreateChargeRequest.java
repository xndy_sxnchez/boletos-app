package com.lsi.boleteria.data.rest.paymentez.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.paymentez.android.model.Card;
import com.paymentez.android.rest.model.User;

public class CreateChargeRequest {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("order")
    @Expose
    private Order order;
    @SerializedName("card")
    @Expose
    private Card card;

    public CreateChargeRequest(){

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
