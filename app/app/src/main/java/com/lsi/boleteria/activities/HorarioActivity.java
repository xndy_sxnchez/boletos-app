package com.lsi.boleteria.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lsi.boleteria.R;
import com.lsi.boleteria.adapters.ListAdapter;
import com.lsi.boleteria.data.entidades.Asiento;
import com.lsi.boleteria.data.entidades.Horario;
import com.lsi.boleteria.data.services.RestUrl;
import com.lsi.boleteria.util.Constantes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class HorarioActivity extends AppCompatActivity {


    private ListView listViewHorarios;
    private ArrayList<Horario> horariosList;
    private ListAdapter listAdapter;
    private Integer destino;
    private Double valor;
    private Long fecha;


    public HorarioActivity() {
        // Required empty public constructor
    }

    Date todayWithZeroTime;

    private Intent intent;
    Toolbar toolbar;
    TextView txtToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_inicio);
        Constantes.hideKeyboardV2(this);

        listViewHorarios = findViewById(R.id.listViewHorarios);

        txtToolbar = findViewById(R.id.txtToolbar);
        toolbar = findViewById(R.id.toolbarHorario);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        intent = getIntent();
        if (intent != null) {
            destino = intent.getIntExtra("DESTINO", 0);
            valor = intent.getDoubleExtra("VALOR", 0.0);
            fecha = intent.getLongExtra("FECHA", 0);
        }
        loadHorarios();

    }

    private void loadHorarios() {

        Constantes.showSimpleProgressDialog(this, "Cargando...", "Horarios", false);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, RestUrl.urlBase +
                "horario/horarios/" + fecha + "/destino/" + destino,
                response -> {
                    try {
                        JSONArray dataHorarios, dataAsientos;
                        JSONObject dataobj, busObj, asientoObj;
                        dataHorarios = new JSONArray(response);
                        Asiento asiento;
                        List<Asiento> asientoList;
                        horariosList = new ArrayList<>();
                        for (int i = 0; i < dataHorarios.length(); i++) {

                            Horario horario = new Horario();
                            dataobj = dataHorarios.getJSONObject(i);
                            busObj = dataobj.getJSONObject("bus");

                            horario.setIdHorario(dataobj.getLong("idHorario"));

                            horario.getBus().setId(busObj.getLong("id"));
                            horario.getBus().setNombre(busObj.getString("nombre"));
                            horario.getBus().setCantidadAsiento(busObj.getInt("cantidadAsiento"));
                            horario.getBus().setNumeroPlaca(busObj.getString("numeroPlaca"));
                            horario.getBus().setUrlImagenBus(busObj.getString("urlImagenBus"));
                            horario.getBus().setConductor(busObj.getString("conductor"));

                            horario.setHoraDescripcion(dataobj.getString("horaDescripcion"));
                            horario.setFechaSalida(dataobj.getString("fechaSalida"));
                            horario.setDestino(dataobj.getString("destino"));
                            dataAsientos = dataobj.getJSONArray("asientoList");
                            asientoList = new ArrayList<>();
                            for (int h = 0; h < dataAsientos.length(); h++) {
                                asientoObj = dataAsientos.getJSONObject(h);
                                asiento = new Asiento();
                                asiento.setValor(valor);
                                asiento.setId(asientoObj.getLong("id"));
                                asiento.setNumeroAsiento(asientoObj.getInt("numeroAsiento"));
                                asiento.setTipoAsiento(asientoObj.getString("tipoAsiento"));
                                asiento.setUbicacion(asientoObj.getString("ubicacion"));
                                asiento.setEstaDisponible(asientoObj.getBoolean("estaDisponible"));
                                asiento.setIdHorario(asientoObj.getInt("idHorario"));
                                asientoList.add(asiento);
                            }
                            horario.setAsientoList(asientoList);
                            horariosList.add(horario);
                        }
                        setupListview();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Toast.makeText(this.getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);


    }

    private void setupListview() {
        Constantes.removeSimpleProgressDialog();  //will remove progress dialog
        if (!horariosList.isEmpty()) {
            listAdapter = new ListAdapter(this, horariosList);
            txtToolbar.setText("Horarios Disponibles: " + horariosList.get(0).getFechaSalida());
            listViewHorarios.setAdapter(listAdapter);
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
