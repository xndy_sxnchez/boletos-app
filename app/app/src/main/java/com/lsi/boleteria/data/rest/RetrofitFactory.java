package com.lsi.boleteria.data.rest;

import android.util.Base64;

import com.lsi.boleteria.data.services.RestUrl;
import com.lsi.boleteria.util.Constantes;
import com.paymentez.android.util.PaymentezUtils;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFactory  {

    private static Retrofit retrofit = null;
    static OkHttpClient.Builder builder = new OkHttpClient().newBuilder();

/*    public static Retrofit getClientPz() {
        *//*if (retrofit == null) {
            String SERVER_URL;
            if (Constantes.PAYMENTEZ_IS_TEST_MODE) {
                SERVER_URL = PaymentezUtils.SERVER_DEV_URL;
            } else {
                SERVER_URL = PaymentezUtils.SERVER_PROD_URL;
            }
            String AUTH_TOKEN = getAuthToken(Constantes.PAYMENTEZ_SERVER_APP_CODE, Constantes.PAYMENTEZ_SERVER_APP_KEY);
            //System.out.println("getAuthToken: " + getAuthToken(Constantes.PAYMENTEZ_SERVER_APP_CODE, Constantes.PAYMENTEZ_SERVER_APP_KEY));
            builder.addInterceptor(new Interceptor() {
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request().newBuilder().addHeader("Content-Type", "application/json")
                            .addHeader("Auth-Token", AUTH_TOKEN).build();
                    return chain.proceed(request);
                }
            });
            OkHttpClient client = builder.build();
            retrofit = (new retrofit2.Retrofit.Builder()).baseUrl(SERVER_URL)
                    .addConverterFactory(GsonConverterFactory.create()).client(client).build();
        }
        return retrofit;*//*
        if (retrofit==null) {
            String SERVER_URL;
            if (Constantes.PAYMENTEZ_IS_TEST_MODE) {
                SERVER_URL = PaymentezUtils.SERVER_DEV_URL;
            } else {
                SERVER_URL = PaymentezUtils.SERVER_PROD_URL;
            }

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            builder.addInterceptor(new Interceptor() {
                @Override public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request().newBuilder().addHeader("Content-Type", "application/json")
                            .addHeader("Auth-Token",
                                    PaymentezUtils.getAuthToken(Constantes.PAYMENTEZ_CLIENT_APP_CODE,
                                            Constantes.PAYMENTEZ_CLIENT_APP_KEY))
                            .build();
                    return chain.proceed(request);
                }
            });
            if (Constantes.PAYMENTEZ_IS_TEST_MODE)
                builder.addInterceptor(logging);

            OkHttpClient client = builder.build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(SERVER_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }*/

    public static Retrofit getClient() {
        //if (retrofit==null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);
            builder.connectTimeout(3, TimeUnit.MINUTES);
            builder.readTimeout(3, TimeUnit.MINUTES);
            builder.writeTimeout(3, TimeUnit.MINUTES);

            OkHttpClient client = builder.build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(RestUrl.paymentezUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        //}
        return retrofit;
    }


}
