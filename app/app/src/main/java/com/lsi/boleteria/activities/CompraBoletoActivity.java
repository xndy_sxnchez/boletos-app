package com.lsi.boleteria.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lsi.boleteria.R;
import com.lsi.boleteria.adapters.AsientosAdapter;
import com.lsi.boleteria.data.entidades.Horario;
import com.lsi.boleteria.util.Constantes;

import java.util.ArrayList;

public class CompraBoletoActivity extends AppCompatActivity {

    private Intent intent;
    private Horario horario;
    private Toolbar toolbar;
    private TextView txtToolbarBus, txtHorarioSalida;

    private LinearLayout linearLayoutBoletos;
    private Button btnAsiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compra_boleto);
        Constantes.hideKeyboardV2(this);

        toolbar = findViewById(R.id.toolBarBoleto);
        setSupportActionBar(toolbar);

        txtToolbarBus = findViewById(R.id.txtToolbarBus);
        txtHorarioSalida = findViewById(R.id.txtHorarioSalida);

        //linearLayoutBoletos = findViewById(R.id.linearLayoutBoletos);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        intent = getIntent();
        if (intent != null) {
            horario = (Horario) intent.getSerializableExtra("HORARIO");
            if (horario != null) {
                txtToolbarBus.setText("Bus #" + horario.getBus().getNombre() + " " + horario.getDestino());
                txtHorarioSalida.setText(horario.getFechaSalida() + " " + horario.getHoraDescripcion());
            }
        }

       /* for (int i = 1; i <= 20; i++) {
            Button myButton = new Button(this);
            myButton.setText("Button :"+i);
            myButton.setId(i);
            final int id_ = myButton.getId();

            LinearLayout layout =  findViewById(R.id.linearLayoutBoletos);
            layout.addView(myButton);

            myButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    System.out.println("hi button");
                }
            });
        }
*/
        ArrayList<String> tipos = new ArrayList<>();
        tipos.add("Ventana");
        tipos.add("Pasillo");
        tipos.add("Pasillo");
        tipos.add("Ventana");
        GridView gridAsientos = findViewById(R.id.gridAsientos);
        GridView gvHeader = findViewById(R.id.gvHeader);
        AsientosAdapter asientosAdapter = new AsientosAdapter(this, new ArrayList<>(horario.getAsientoList()));

        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.header_bus, R.id.txtUno, tipos);

        gridAsientos.setAdapter(asientosAdapter);
        gvHeader.setAdapter(adapter);

        ViewCompat.setNestedScrollingEnabled(gridAsientos, true);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
