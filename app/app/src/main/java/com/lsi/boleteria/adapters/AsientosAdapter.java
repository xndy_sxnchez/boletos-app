package com.lsi.boleteria.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.lsi.boleteria.activities.PagoBoletoActivity;
import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Asiento;

import java.util.ArrayList;

public class AsientosAdapter extends BaseAdapter {

    private Intent intent;
    private Context context;
    private ArrayList<Asiento> asientoArrayList;

    public AsientosAdapter(Context context, ArrayList<Asiento> asientoArrayList) {

        this.context = context;
        this.asientoArrayList = asientoArrayList;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {
        if (asientoArrayList != null)
            return asientoArrayList.size();
        else
            return 0;
    }

    @Override
    public Asiento getItem(int position) {
        return asientoArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Button btnUno, btnDos, btnTres, btnCuatro;
        //ImageView imgSeparador;
        //if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.bus_asientos, parent, false);
            ///holder = new ViewHolder();

            //  convertView.setTag(holder);
        //} /*else {
            // the getTag returns the viewHolder object set as a tag to the view
            //holder = (ViewHolder) convertView.getTag();
        //}*/

        btnUno = convertView.findViewById(R.id.btnUno);
        btnDos = convertView.findViewById(R.id.btnDos);
        btnTres = convertView.findViewById(R.id.btnTres);
        btnCuatro = convertView.findViewById(R.id.btnCuatro);
        //imgSeparador = convertView.findViewById(R.id.imgSeparador);

        //LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100, 100);
        //imgSeparador.setLayoutParams(layoutParams);


        Asiento asiento = getItem(position);

        btnUno.setText(asiento.getNumeroAsiento().toString());
        btnDos.setText(asiento.getNumeroAsiento().toString());
        btnTres.setText(asiento.getNumeroAsiento().toString());
        btnCuatro.setText(asiento.getNumeroAsiento().toString());

        btnUno.setBackground(ContextCompat.getDrawable(context, R.drawable.button_bg));
        btnDos.setBackground(ContextCompat.getDrawable(context, R.drawable.button_bg));
        btnTres.setBackground(ContextCompat.getDrawable(context, R.drawable.button_bg));
        btnCuatro.setBackground(ContextCompat.getDrawable(context, R.drawable.button_bg));

        if (asiento.getEstaDisponible()) {
            btnUno.setBackgroundColor(Color.WHITE);
            btnDos.setBackgroundColor(Color.WHITE);
            btnTres.setBackgroundColor(Color.WHITE);
            btnCuatro.setBackgroundColor(Color.WHITE);
        } else {
            btnUno.setBackgroundColor(Color.RED);
            btnDos.setBackgroundColor(Color.RED);
            btnTres.setBackgroundColor(Color.RED);
            btnCuatro.setBackgroundColor(Color.RED);
        }

        btnUno.setTextColor(Color.BLACK);
        btnDos.setTextColor(Color.BLACK);
        btnTres.setTextColor(Color.BLACK);
        btnCuatro.setTextColor(Color.BLACK);

        btnUno.setOnClickListener(view -> {
            if(asiento.getEstaDisponible()){
                intent = new Intent(context, PagoBoletoActivity.class);
                intent.putExtra("ASIENTO", asiento);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }else{
                Toast.makeText(context, "Asiento no esta disponible", Toast.LENGTH_SHORT).show();
            }

        });

        btnDos.setOnClickListener(view -> {
            if(asiento.getEstaDisponible()){
                intent = new Intent(context, PagoBoletoActivity.class);
                intent.putExtra("ASIENTO", asiento);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }else{
                Toast.makeText(context, "Asiento no esta disponible", Toast.LENGTH_SHORT).show();
            }
        });

        btnTres.setOnClickListener(view -> {
            if(asiento.getEstaDisponible()){
                intent = new Intent(context, PagoBoletoActivity.class);
                intent.putExtra("ASIENTO", asiento);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }else{
                Toast.makeText(context, "Asiento no esta disponible", Toast.LENGTH_SHORT).show();
            }
        });


        btnCuatro.setOnClickListener(view -> {
            if(asiento.getEstaDisponible()){
                intent = new Intent(context, PagoBoletoActivity.class);
                intent.putExtra("ASIENTO", asiento);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }else{
                Toast.makeText(context, "Asiento no esta disponible", Toast.LENGTH_SHORT).show();
            }
        });


        return convertView;
    }

    private class ViewHolder {


        protected Button btnUno, btnDos, btnTres, btnCuatro;

    }

}