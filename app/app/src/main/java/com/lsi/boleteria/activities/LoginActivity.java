package com.lsi.boleteria.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.lsi.boleteria.R;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.services.Services;
import com.lsi.boleteria.util.Constantes;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Services services;

    private Button btnIniciarSesion;
    private TextView txtRegistro, txtOlvidoContrasenia;
    private EditText editTextNombreUsuario, editTextContrasenia;
    private ProgressDialog progressDialog;
    private Usuario usuario;
    private Intent mainIntent;
    private String validado = "NO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        Constantes.hideKeyboardV2(this);

        services = new Services();
        progressDialog = new ProgressDialog(this);

        editTextNombreUsuario = findViewById(R.id.editTextNombreUsuario);
        editTextContrasenia = findViewById(R.id.editTextContrasenia);


        btnIniciarSesion = findViewById(R.id.btnIniciarSesion);
        txtRegistro = findViewById(R.id.txtRegistro);
        txtOlvidoContrasenia = findViewById(R.id.txtOlvidoContrasenia);

        txtRegistro.setOnClickListener(this);
        txtOlvidoContrasenia.setOnClickListener(this);
        btnIniciarSesion.setOnClickListener(this);
    }

    private Boolean validarDatos() {
        if (editTextNombreUsuario.getText().toString() != null && editTextNombreUsuario.getText().toString().length() < 3) {
            Toast.makeText(this, "Ingrese un nombre de Usuario Correcto", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (editTextContrasenia.getText().toString() != null && editTextContrasenia.getText().toString().length() <= 4) {
            Toast.makeText(this, "Ingrese una Contraseña Correcta", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnIniciarSesion:
                if (validarDatos()) {
                    usuario = new Usuario(editTextNombreUsuario.getText().toString(), editTextContrasenia.getText().toString());
                    new AsyncTask<Void, Void, Boolean>() {
                        @Override
                        protected Boolean doInBackground(Void... params) {
                            try {
                                return services.login(usuario);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return false;
                        }

                        @Override
                        protected void onPostExecute(Boolean result) {
                            progressDialog.dismiss();
                            if (result) nextNextActivity();
                            else incorrectPassWord();
                        }

                        @Override
                        protected void onProgressUpdate(Void... values) {
                            super.onProgressUpdate(values);
                        }

                        @Override
                        protected void onPreExecute() {

                            progressDialog.setMax(100);
                            progressDialog.setMessage("Cargando ...");
                            progressDialog.setTitle(R.string.iniciando_sesion);
                            progressDialog.setCancelable(false);
                            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressDialog.show();
                            super.onPreExecute();
                        }
                    }.execute();
                }


                break;
            case R.id.txtRegistro:
                mainIntent = new Intent(LoginActivity.this, DatosRegistroActivity.class);
                LoginActivity.this.startActivity(mainIntent);
                break;
            case R.id.txtOlvidoContrasenia:
                mainIntent = new Intent(LoginActivity.this, OlvidoContraseniaActivity.class);
                LoginActivity.this.startActivity(mainIntent);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void incorrectPassWord() {
        Toast.makeText(this, R.string.usuarios_incorrectos, Toast.LENGTH_SHORT).show();
        editTextContrasenia.setText("");
    }

    void nextNextActivity() {
        mainIntent = new Intent(LoginActivity.this, HomeActivity.class);
        LoginActivity.this.startActivity(mainIntent);
        finish();
    }




}
