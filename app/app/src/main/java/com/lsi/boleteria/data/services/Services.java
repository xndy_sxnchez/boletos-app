package com.lsi.boleteria.data.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lsi.boleteria.BoleteriaApp;
import com.lsi.boleteria.data.entidades.Boleto;
import com.lsi.boleteria.data.entidades.DateDeserializer;
import com.lsi.boleteria.data.entidades.Usuario;
import com.lsi.boleteria.data.models.BoletoModel;
import com.lsi.boleteria.data.models.BoletoResponse;
import com.lsi.boleteria.data.models.UsuarioModel;
import com.lsi.boleteria.data.preferences.SharedPreferencesBoleteria;
import com.lsi.boleteria.util.Constantes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Services {

    private JSONObject jsonObject;
    private RequestFuture<JSONObject> future;
    private GsonBuilder gsonBuilder;
    private Gson gson;
    private static int responseCode;
    private SharedPreferencesBoleteria sharedPreferencesBoleteria;

    public Services() {
        sharedPreferencesBoleteria = SharedPreferencesBoleteria.getInstance(BoleteriaApp.getAppContext());
    }

    public static Boolean checkConection() {
        ConnectivityManager cm =
                (ConnectivityManager) BoleteriaApp.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }


    public Boolean login(Usuario usuario) {
        sharedPreferencesBoleteria.clearKeys();
        future = RequestFuture.newFuture();
        JSONObject js = new JSONObject();

        gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        gson = gsonBuilder.create();
        Map<String, String> jsonParams = new HashMap<>();
        jsonParams.put("data", gson.toJson(usuario));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, RestUrl.iniciarSesion, new JSONObject(jsonParams), future, future) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        BoleteriaApp.getInstance().addToRequestQueue(request);
        try {
            jsonObject = future.get(Constantes.timeOut, TimeUnit.SECONDS);
            Usuario jsonUsuario = gson.fromJson(jsonObject.toString(), Usuario.class);
            if (jsonUsuario != null) {
                //GUARDO USUARIO
                if (jsonUsuario.getId() != null) {
                    sharedPreferencesBoleteria.saveKey(Constantes.USER, jsonObject.toString());
                    return true;
                }
                ///NO GUARDO EL USUARIO
                else return false;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            // exception handling
            //  Log.i(TAG, " InterruptedException  " + e);
            return false;
        }
        return false;
    }


    public UsuarioModel existeUsuario(String nombreUsuario) {
        future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, RestUrl.existeUsuario + nombreUsuario, new JSONObject(), future, future) {
        };
        BoleteriaApp.getInstance().addToRequestQueue(request);
        try {
            jsonObject = future.get(Constantes.timeOut, TimeUnit.SECONDS);
            gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
            gson = gsonBuilder.create();
            UsuarioModel usuarioModel = gson.fromJson(jsonObject.toString(), UsuarioModel.class);
            return usuarioModel;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            return null;
        }
    }

    public Boolean crearUsuario(Usuario usuario) {
        sharedPreferencesBoleteria.clearKeys();
        future = RequestFuture.newFuture();
        gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        gson = gsonBuilder.create();
        Map<String, String> jsonParams = new HashMap<>();
        jsonParams.put("data", gson.toJson(usuario));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, RestUrl.crearUsuario, new JSONObject(jsonParams), future, future) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        BoleteriaApp.getInstance().addToRequestQueue(request);
        try {
            jsonObject = future.get(Constantes.timeOut, TimeUnit.SECONDS);
            Usuario jsonUsuario = gson.fromJson(jsonObject.toString(), Usuario.class);
            if (jsonUsuario != null) {
                //GUARDO USUARIO
                if (jsonUsuario.getId() != null) {
                    sharedPreferencesBoleteria.saveKey(Constantes.USER, jsonObject.toString());
                    return true;
                }
                ///NO GUARDO EL USUARIO
                else return false;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            // exception handling
            //  Log.i(TAG, " InterruptedException  " + e);
            return false;
        }
        return false;
    }

    public Usuario getUsuario() {
        Usuario usuario = null;
        gson = new Gson();
        if (sharedPreferencesBoleteria.getKey(Constantes.USER) != null) {
            usuario = gson.fromJson(sharedPreferencesBoleteria.getKey(Constantes.USER), Usuario.class);
        }
        return usuario;
    }


    public Boleto guardarBoleto(BoletoModel boleto) {
        future = RequestFuture.newFuture();
        gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        gson = gsonBuilder.create();
        Map<String, String> jsonParams = new HashMap<>();
        jsonParams.put("data", gson.toJson(boleto));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, RestUrl.guardarBoleto, new JSONObject(jsonParams), future, future) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        BoleteriaApp.getInstance().addToRequestQueue(request);
        try {
            jsonObject = future.get(Constantes.timeOut, TimeUnit.SECONDS);
            Boleto boletoResponse = gson.fromJson(jsonObject.toString(), Boleto.class);
            if (boletoResponse != null) {
                //GUARDO USUARIO
                if (boletoResponse.getId() != null) {
                    return boletoResponse;
                }
                ///NO GUARDO EL USUARIO
                else return new Boleto();
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            // exception handling
            //  Log.i(TAG, " InterruptedException  " + e);
            return new Boleto();
        }
        return new Boleto();
    }


    public BoletoResponse findBoleto(Long numeroBoleto) {
        future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, RestUrl.numBoleto + numeroBoleto, new JSONObject(), future, future) {
        };
        BoleteriaApp.getInstance().addToRequestQueue(request);
        try {
            jsonObject = future.get(Constantes.timeOut, TimeUnit.SECONDS);
            gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
            gson = gsonBuilder.create();
            BoletoResponse boletoResponse = gson.fromJson(jsonObject.toString(), BoletoResponse.class);
            return boletoResponse;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            return null;
        }
    }

    public Boolean actualizarUsuario(Usuario usuario) {

        future = RequestFuture.newFuture();
        gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        gson = gsonBuilder.create();
        Map<String, String> jsonParams = new HashMap<>();
        jsonParams.put("data", gson.toJson(usuario));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, RestUrl.crearUsuario, new JSONObject(jsonParams), future, future) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        BoleteriaApp.getInstance().addToRequestQueue(request);
        try {
            jsonObject = future.get(Constantes.timeOut, TimeUnit.SECONDS);
            Usuario jsonUsuario = gson.fromJson(jsonObject.toString(), Usuario.class);
            if (jsonUsuario != null) {
                //GUARDO USUARIO
                if (jsonUsuario.getId() != null) {
                    sharedPreferencesBoleteria.clearKeys();
                    sharedPreferencesBoleteria.saveKey(Constantes.USER, jsonObject.toString());
                    return true;
                }
                ///NO GUARDO EL USUARIO
                else return false;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            // exception handling
            //  Log.i(TAG, " InterruptedException  " + e);
            return false;
        }
        return false;
    }

    public void recuperarContrasenia(Long idUser) {
        future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                RestUrl.recuperarUsuario + idUser, new JSONObject(), future, future) {
        };
        BoleteriaApp.getInstance().addToRequestQueue(request);
        try {
            future.get(Constantes.timeOut, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public void cambiarContrasenia(Long idUser, String contraseniaNueva) {

        future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                RestUrl.cambiarContrasenia + idUser + "/contraseniaNueva/" + contraseniaNueva, new JSONObject(), future, future) {
        };
        BoleteriaApp.getInstance().addToRequestQueue(request);
        try {
            future.get(Constantes.timeOut, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }


}
