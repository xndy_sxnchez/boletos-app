package com.lsi.boleteria.controller;

import com.google.common.collect.Lists;
import com.lsi.boleteria.configs.ApiRest;
import com.lsi.boleteria.configs.Constantes;
import com.lsi.boleteria.entidades.Asiento;
import com.lsi.boleteria.entidades.Boleto;
import com.lsi.boleteria.entidades.Horario;
import com.lsi.boleteria.models.BusModel;
import com.lsi.boleteria.models.HorarioModel;
import com.lsi.boleteria.models.SortbyAsiento;
import com.lsi.boleteria.repository.AsientoRepository;
import com.lsi.boleteria.repository.BoletoRepository;
import com.lsi.boleteria.repository.HorarioRepository;
import org.apache.commons.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(ApiRest.horario)
public class HorarioController {

    @Autowired
    private HorarioRepository horarioRepository;

    @Autowired
    private AsientoRepository asientoRepository;
    @Autowired
    private BoletoRepository boletoRepository;

    @RequestMapping(value = "/horarios/{fechaSalida}/destino/{idDestino}", method = RequestMethod.GET)
    public List<HorarioModel> findAllHorarios(@PathVariable Long fechaSalida, @PathVariable Long idDestino) {
        System.out.println("fechaSalida: " + fechaSalida);
        System.out.println("idDestino: " + idDestino);
        List<Horario> horarios =
                Lists.newArrayList(horarioRepository.findAllByFechaSalidaAndDestino_IdOrderByHoraSalida(new Date(fechaSalida), idDestino));
        List<HorarioModel> horarioModels = new ArrayList<>();
        List<Asiento> asientos = new ArrayList<>();
        HorarioModel horarioModel;
        BusModel busModel;
        if (!horarios.isEmpty()) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
            for (Horario h : horarios) {
                busModel = new BusModel(h.getBus().getId(), h.getBus().getNombre(), h.getBus().getCantidadAsiento(),
                        h.getBus().getNumeroPlaca(), h.getBus().getUrlImagenBus(), h.getBus().getConductor());
                horarioModel = new HorarioModel(h.getId(), busModel,
                        h.getHoraSalida().toString().substring(0, h.getHoraSalida().toString().length() - 3),
                        dateFormat.format(h.getFechaSalida()), WordUtils.capitalize(h.getDestino().getDescripcion().toLowerCase()),
                        h.getDestino().getUrlImagen(), h.getBus().getSucursal().getDireccion());
                horarioModels.add(horarioModel);
                asientos = asientoRepository.findAllByBusId(horarioModel.getBus().getId());
                if (asientos != null || !asientos.isEmpty()) {
                    Boleto boleto;
                    Collections.sort(asientos, new SortbyAsiento());
                    for(Asiento asiento : asientos){
                        asiento.setIdHorario(h.getId().intValue());
                        boleto = boletoRepository.findByAsientoIdAndHorarioId(asiento.getId(), h.getId());
                        if(boleto != null){
                            asiento.setEstaDisponible(Boolean.FALSE);
                        }
                    }
                    horarioModel.setAsientoList(asientos);
                }
            }
        }

        return horarioModels;
    }

    private   String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

}
