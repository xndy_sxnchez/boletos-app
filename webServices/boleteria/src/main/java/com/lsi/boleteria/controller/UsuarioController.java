/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.controller;

import com.google.gson.Gson;
import com.lsi.boleteria.async.EmailService;
import com.lsi.boleteria.configs.ApiRest;
import com.lsi.boleteria.configs.Constantes;
import com.lsi.boleteria.entidades.Cliente;
import com.lsi.boleteria.entidades.Usuario;
import com.lsi.boleteria.models.DataModel;
import com.lsi.boleteria.models.UsuarioModel;
import com.lsi.boleteria.repository.ClienteRepository;
import com.lsi.boleteria.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Juan Carlos
 */
@RestController
@RequestMapping(ApiRest.usuario)
public class UsuarioController {

    @Autowired
    private EmailService emailService;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private ClienteRepository clienteRepository;

    @RequestMapping(value = ApiRest.iniciarSesion, method = RequestMethod.POST)
    public Usuario iniciarSesion(HttpEntity<String> httpEntity) {

        Gson gson = new Gson();
        DataModel data = gson.fromJson(httpEntity.getBody(), DataModel.class);
        try {
            Usuario usuarioLogin = gson.fromJson(data.getData(), Usuario.class);
            Usuario usuario = usuarioRepository.findByNombreUsuarioAndContrasenaAndEstado(usuarioLogin.getNombreUsuario(),
                    usuarioLogin.getContrasena(), usuarioLogin.getEstado());
            if (usuario != null) {
                return usuario;
            } else {
                return new Usuario();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Usuario();
        }
    }

    @RequestMapping(value = ApiRest.existeUsuario, method = RequestMethod.GET)
    public UsuarioModel existeUsuario(@PathVariable String nombreUsuario) {
        UsuarioModel usuarioModel = null;
        Usuario usuario = usuarioRepository.findByNombreUsuario(nombreUsuario);
        if (usuario != null) {
            usuarioModel = new UsuarioModel(usuario.getId(), usuario.getCliente().getApellido() + " " +
                    usuario.getCliente().getNombre(), usuario.getNombreUsuario(), usuario.getCliente().getCorreo());
        }
        return usuarioModel;
    }

    @RequestMapping(value = ApiRest.recuperarContrasenia, method = RequestMethod.GET)
    public Usuario recuperarContrasenia(@PathVariable Long idUsuario) {
        Usuario usuario = usuarioRepository.findById(idUsuario).get();
        usuario.setContrasena(usuario.getNombreUsuario() + "2019");
        usuarioRepository.save(usuario);
        emailService.sendMailUsuario(usuario);

        return usuario;
    }

    @RequestMapping(value = ApiRest.cambiarContrasenia, method = RequestMethod.GET)
    public Usuario cambiarContrasenia(@PathVariable Long idUsuario, @PathVariable String contraseniaNueva) {
        Usuario usuario = usuarioRepository.findById(idUsuario).get();
        usuario.setContrasena(contraseniaNueva);
        usuarioRepository.save(usuario);
        return usuario;
    }

    @RequestMapping(value = ApiRest.guardarUsuario, method = RequestMethod.POST)
    public Usuario guardarUsuario(HttpEntity<String> httpEntity) {

        Gson gson = new Gson();
        DataModel data = gson.fromJson(httpEntity.getBody(), DataModel.class);
        try {

            Usuario usuarioRegistro = gson.fromJson(data.getData(), Usuario.class);
            System.out.println("usuarioRegistro " + usuarioRegistro.toString());
            //usuarioRegistro = usuarioRepository.save(usuarioRegistro);
            usuarioRegistro.setEstado(Boolean.TRUE);
            usuarioRegistro.setEsAdmin(Boolean.FALSE);
            Cliente cliente = usuarioRegistro.getCliente();
            if (usuarioRegistro.getId() != null) {
                Cliente temp = usuarioRepository.findById(usuarioRegistro.getId()).get().getCliente();
                if (temp != null)
                    cliente.setId(temp.getId());
            }
            cliente = clienteRepository.save(cliente);
            usuarioRegistro.setCliente(cliente);
            return usuarioRepository.save(usuarioRegistro);
        } catch (Exception e) {
            e.printStackTrace();
            return new Usuario();
        }
    }


}
