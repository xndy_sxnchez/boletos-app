package com.lsi.boleteria.models;

import com.lsi.boleteria.entidades.Asiento;

import java.util.Comparator;

public class SortbyAsiento  implements Comparator<Asiento> {
    // Used for sorting in ascending order of
    // roll number
    public int compare(Asiento a, Asiento b) {
        return a.getNumeroAsiento() - b.getNumeroAsiento();
    }
}