/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.repository;

import com.lsi.boleteria.entidades.Horario;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Juan Carlos
 */
public interface HorarioRepository extends CrudRepository<Horario, Long> {

    List<Horario> findAllByFechaSalidaAndDestino_IdOrderByHoraSalida(Date fechaSalida, Long idDestino);

}
