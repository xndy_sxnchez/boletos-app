package com.lsi.boleteria.async;

import com.lsi.boleteria.entidades.Boleto;
import com.lsi.boleteria.entidades.FormatoNotificacion;
import com.lsi.boleteria.entidades.Usuario;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class EmailService {

    @Async
    public void sendMail(FormatoNotificacion formatoNotificacion, Boleto boleto) {
        try {
            //System.out.println("correo " + correoContribuyente);
            Email correo = new Email(boleto, formatoNotificacion.getHeader(), formatoNotificacion.getMensaje());
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            Future<Boolean> future = executorService.submit(() -> {
              //  Thread.sleep(2000);
                return correo.sendMail();
            });
            while (!future.isDone()) {
                //System.out.println("sending mail");
            }

            System.out.println("sended mail");

        } catch (Exception e) {
            Logger.getLogger(EmailService.class.getName()).log(Level.SEVERE, null, e);
        }
    }


    @Async
    public void sendMailUsuario( Usuario u) {
        try {
            Email correo = new Email(u);
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            Future<Boolean> future = executorService.submit(() -> {
                //  Thread.sleep(2000);
                return correo.sendMail();
            });
            while (!future.isDone()) {
                //System.out.println("sending mail");
            }

            System.out.println("sended mail");

        } catch (Exception e) {
            Logger.getLogger(EmailService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
