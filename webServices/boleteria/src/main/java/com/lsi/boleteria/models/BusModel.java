package com.lsi.boleteria.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lsi.boleteria.entidades.Horario;
import com.lsi.boleteria.entidades.Sucursal;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

public class BusModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String nombre;
    private Integer cantidadAsiento;
    private String numeroPlaca;
    private String urlImagenBus;
    private String conductor;

    public BusModel() {
    }

    public BusModel(Long id, String nombre, Integer cantidadAsiento, String numeroPlaca, String urlImagenBus, String conductor) {
        this.id = id;
        this.nombre = nombre;
        this.cantidadAsiento = cantidadAsiento;
        this.numeroPlaca = numeroPlaca;
        this.urlImagenBus = urlImagenBus;
        this.conductor = conductor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCantidadAsiento() {
        return cantidadAsiento;
    }

    public void setCantidadAsiento(Integer cantidadAsiento) {
        this.cantidadAsiento = cantidadAsiento;
    }

    public String getNumeroPlaca() {
        return numeroPlaca;
    }

    public void setNumeroPlaca(String numeroPlaca) {
        this.numeroPlaca = numeroPlaca;
    }

    public String getUrlImagenBus() {
        return urlImagenBus;
    }

    public void setUrlImagenBus(String urlImagenBus) {
        this.urlImagenBus = urlImagenBus;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }
}
