/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Juan Carlos
 */
@Entity
@Table(name = "bus", schema = "aplicacion")
public class Bus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
        @JoinColumn(name = "sucursal", referencedColumnName = "Id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Sucursal sucursal;
    @Column(name = "nombre", length = 50)
    private String nombre;
    @Column(name = "cantidad_asiento")
    private Integer cantidadAsiento;
    @Column(name = "numero_placa", length = 10)
    private String numeroPlaca;
    @Column(name = "url_imagen_bus")
    private String urlImagenBus;
    @Column(name = "conductor", length = 50)
    private String conductor;
    @JsonIgnore
    @OneToMany(mappedBy = "bus", fetch = FetchType.EAGER)
    private List<Horario> horarioList;


    public Bus() {
    }

    public Bus(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCantidadAsiento() {
        return cantidadAsiento;
    }

    public void setCantidadAsiento(Integer cantidadAsiento) {
        this.cantidadAsiento = cantidadAsiento;
    }

    public String getNumeroPlaca() {
        return numeroPlaca;
    }

    public void setNumeroPlaca(String numeroPlaca) {
        this.numeroPlaca = numeroPlaca;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public List<Horario> getHorarioList() {
        return horarioList;
    }

    public void setHorarioList(List<Horario> horarioList) {
        this.horarioList = horarioList;
    }


    public String getUrlImagenBus() {
        return urlImagenBus;
    }

    public void setUrlImagenBus(String urlImagenBus) {
        this.urlImagenBus = urlImagenBus;
    }

    @Override
    public String toString() {
        return "com.lsi.boleteria.entidades.Bus[ id=" + id + " ]";
    }
    
}
