package com.lsi.boleteria.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lsi.boleteria.entidades.Asiento;
import com.lsi.boleteria.entidades.Bus;

import java.io.Serializable;
import java.util.List;

//@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class HorarioModel implements Serializable {

    private Long idHorario;
    private BusModel bus;
    private String horaDescripcion;
    private String fechaSalida;
    private String destino;
    private String sucursal;
    private String urlDestinoImagen;
    private List<Asiento> asientoList;

    public HorarioModel(Long idHorario, BusModel bus, String horaDescripcion, String fechaSalida,
                        String destino, String urlDestinoImagen, String sucursal) {
        this.idHorario = idHorario;
        this.bus = bus;
        this.horaDescripcion = horaDescripcion;
        this.fechaSalida = fechaSalida;
        this.destino = destino;
        this.urlDestinoImagen = urlDestinoImagen;
        this.sucursal = sucursal;
    }

    public HorarioModel() {
    }

    public Long getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Long idHorario) {
        this.idHorario = idHorario;
    }

    public BusModel getBus() {
        return bus;
    }

    public void setBus(BusModel bus) {
        this.bus = bus;
    }

    public String getHoraDescripcion() {
        return horaDescripcion;
    }

    public void setHoraDescripcion(String horaDescripcion) {
        this.horaDescripcion = horaDescripcion;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getUrlDestinoImagen() {
        return urlDestinoImagen;
    }

    public void setUrlDestinoImagen(String urlDestinoImagen) {
        this.urlDestinoImagen = urlDestinoImagen;
    }

    public List<Asiento> getAsientoList() {
        return asientoList;
    }

    public void setAsientoList(List<Asiento> asientoList) {
        this.asientoList = asientoList;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }
}
