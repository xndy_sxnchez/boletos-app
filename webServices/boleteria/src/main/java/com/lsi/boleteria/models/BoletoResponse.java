package com.lsi.boleteria.models;

public class BoletoResponse {

    private Integer id;
    private Integer idCliente;
    private String fechaCompra;
    private String fechaSalida;
    private String asiento;
    private String horario;
    private String destino;
    private String claveAcceso;
    private String numAutorizacion;
    private String numFactura;
    private String usuarioEmite;
    private String total;
    private String bus;
    private Long numeroBoleto;
    private Integer anio;
    private String numeroBoletoFormato;

    public BoletoResponse() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getAsiento() {
        return asiento;
    }

    public void setAsiento(String asiento) {
        this.asiento = asiento;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getClaveAcceso() {
        return claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    public String getNumAutorizacion() {
        return numAutorizacion;
    }

    public void setNumAutorizacion(String numAutorizacion) {
        this.numAutorizacion = numAutorizacion;
    }

    public String getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(String numFactura) {
        this.numFactura = numFactura;
    }

    public String getUsuarioEmite() {
        return usuarioEmite;
    }

    public void setUsuarioEmite(String usuarioEmite) {
        this.usuarioEmite = usuarioEmite;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public Long getNumeroBoleto() {
        return numeroBoleto;
    }

    public void setNumeroBoleto(Long numeroBoleto) {
        this.numeroBoleto = numeroBoleto;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public String getNumeroBoletoFormato() {
        return numeroBoletoFormato;
    }

    public void setNumeroBoletoFormato(String numeroBoletoFormato) {
        this.numeroBoletoFormato = numeroBoletoFormato;
    }
}
