/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.repository;

import com.lsi.boleteria.entidades.Asiento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 *
 * @author Juan Carlos
 */
public interface AsientoRepository extends CrudRepository<Asiento, Long> {


    List<Asiento> findAllByBusId(Long idBus);
    
}
