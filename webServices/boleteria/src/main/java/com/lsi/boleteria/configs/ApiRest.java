package com.lsi.boleteria.configs;

public class ApiRest {

    public static final String urlBase = "/boleteria/api";


    //Usuarios Controller
    public static final String usuario = urlBase + "/usuario";

    public static final String iniciarSesion = "/iniciarSesion";
    public static final String existeUsuario = "nombreUsuario/{nombreUsuario}";
    public static final String recuperarContrasenia = "idUsuario/{idUsuario}";
    public static final String cambiarContrasenia = "idUsuario/{idUsuario}/contraseniaNueva/{contraseniaNueva}";
    public static final String guardarUsuario = "/registro";

    //Horarios
        public static final String horario = urlBase +  "/horario";
    public static final String horarios = "/horarios/{fechaSalida}";

    //boletos
    public static final String boleto = urlBase +  "/boleto";
    public static final String guardarBoleto =  "/guardarBoleto";
    public static final String boletos =  "/boletos/{idCliente}";
    public static final String numBoleto =  "/numBoleto/{numeroBoleto}";

}
