package com.lsi.boleteria.controller;

import com.google.common.collect.Lists;
import com.lsi.boleteria.configs.ApiRest;
import com.lsi.boleteria.entidades.Bus;
import com.lsi.boleteria.repository.BusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class BusController {

    @Autowired
    private BusRepository busRepository;


    @RequestMapping(value = ApiRest.horarios, method = RequestMethod.GET)
    public List<Bus> findAllHorarios() {
        List<Bus> buses = Lists.newArrayList(busRepository.findAll());
        return buses;
    }

}
