package com.lsi.boleteria.controller;

import com.google.gson.Gson;
import com.lsi.boleteria.async.EmailService;
import com.lsi.boleteria.configs.ApiRest;
import com.lsi.boleteria.entidades.Boleto;
import com.lsi.boleteria.entidades.Cliente;
import com.lsi.boleteria.entidades.FormatoNotificacion;
import com.lsi.boleteria.entidades.Usuario;
import com.lsi.boleteria.models.BoletoModel;
import com.lsi.boleteria.models.BoletoResponse;
import com.lsi.boleteria.models.DataModel;
import com.lsi.boleteria.repository.AsientoRepository;
import com.lsi.boleteria.repository.BoletoRepository;
import com.lsi.boleteria.repository.ClienteRepository;
import com.lsi.boleteria.repository.FormatoNotificacionRepository;
import com.lsi.boleteria.repository.HorarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(ApiRest.boleto)
public class BoletosController {

    @Autowired
    private BoletoRepository boletoRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private HorarioRepository horarioRepository;
    @Autowired
    private AsientoRepository asientoRepository;
    @Autowired
    private FormatoNotificacionRepository formatoNotificacionRepository;
    @Autowired
    private EmailService emailService;

    @RequestMapping(value = ApiRest.guardarBoleto, method = RequestMethod.POST)
    public BoletoResponse guardarBoleto(HttpEntity<String> httpEntity) {

        Gson gson = new Gson();
        DataModel data = gson.fromJson(httpEntity.getBody(), DataModel.class);
        try {

            Long temp = boletoRepository.count();

            Boleto boleto = gson.fromJson(data.getData(), Boleto.class);
            BoletoModel boletoModel = gson.fromJson(data.getData(), BoletoModel.class);
            boleto.setCliente(clienteRepository.findById(boletoModel.getIdCliente().longValue()).get());
            boleto.setHorario(horarioRepository.findById(boletoModel.getIdHorario().longValue()).get());
            boleto.setFechaCompra(new Date());
            boleto.setAsiento(asientoRepository.findById(boletoModel.getIdAsiento().longValue()).get());
            if (temp != null) {
                boleto.setNumeroBoleto(temp + 1L);
            } else {
                boleto.setNumeroBoleto(1L);
            }
            boleto.setNumeroBoletoFormato(boletoModel.getValor());
            boleto.setEstado(new Date().getYear());
            boleto.setFechaSalida(horarioRepository.findById(boletoModel.getIdHorario().longValue()).get().getFechaSalida());
            boleto = boletoRepository.save(boleto);
            BoletoResponse boletoResponse = new BoletoResponse();
            boletoResponse.setAnio(boleto.getEstado());
            boletoResponse.setAsiento(boleto.getAsiento().getNumeroAsiento().toString());
            boletoResponse.setId(boleto.getId().intValue());
            boletoResponse.setIdCliente(boleto.getCliente().getId().intValue());
            boletoResponse.setNumeroBoleto(boleto.getNumeroBoleto());
            try {
                formatoNotificacionRepository.findById(1L).get();

                emailService.sendMail(formatoNotificacionRepository.findById(1L).get(), boleto);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return boletoResponse;
        } catch (Exception e) {
            e.printStackTrace();
            return new BoletoResponse();
        }
    }

    @RequestMapping(value = ApiRest.boletos, method = RequestMethod.GET)
    public List<BoletoResponse> findAllBoletos(@PathVariable Long idCliente) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            List<BoletoResponse> boletoResponses = new ArrayList<>();
            for (Boleto boleto : boletoRepository.findAllByCliente_Id(idCliente)) {
                BoletoResponse boletoResponse = new BoletoResponse();
                boletoResponse.setAnio(boleto.getEstado());
                boletoResponse.setAsiento(boleto.getAsiento().getNumeroAsiento().toString());
                boletoResponse.setId(boleto.getId().intValue());
                boletoResponse.setIdCliente(boleto.getCliente().getId().intValue());
                boletoResponse.setBus(boleto.getAsiento().getBus().getNombre());
                boletoResponse.setUsuarioEmite(boleto.getCliente().getApellido() + " " + boleto.getCliente().getNombre());
                boletoResponse.setTotal(boleto.getNumeroBoletoFormato());
                boletoResponse.setFechaSalida(dateFormat.format(boleto.getHorario().getFechaSalida()) + " " +
                        (boleto.getHorario().getHoraSalida().toString().substring(0, boleto.getHorario().getHoraSalida().toString().length() - 3)));

                boletoResponse.setNumeroBoleto(boleto.getNumeroBoleto());
                boletoResponse.setDestino(boleto.getHorario().getDestino().getNombre());
                if (boleto.getCliente().getCedula() != null) {
                    boletoResponse.setNumeroBoletoFormato(boleto.getCliente().getCedula());
                } else {
                    boletoResponse.setNumeroBoletoFormato("");
                }

                boletoResponses.add(boletoResponse);

            }
            return boletoResponses;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();

        }
    }

    @RequestMapping(value = ApiRest.numBoleto, method = RequestMethod.GET)
    public BoletoResponse findBoleto(@PathVariable Long numeroBoleto) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Boleto boleto = boletoRepository.findByNumeroBoleto(numeroBoleto);
            BoletoResponse boletoResponse = new BoletoResponse();
            boletoResponse.setAnio(boleto.getEstado());
            boletoResponse.setAsiento(boleto.getAsiento().getNumeroAsiento().toString());
            boletoResponse.setId(boleto.getId().intValue());
            boletoResponse.setIdCliente(boleto.getCliente().getId().intValue());
            boletoResponse.setBus(boleto.getAsiento().getBus().getNombre());
            boletoResponse.setUsuarioEmite(boleto.getCliente().getApellido() + " " + boleto.getCliente().getNombre());
            boletoResponse.setTotal(boleto.getNumeroBoletoFormato());
            boletoResponse.setNumeroBoleto(boleto.getNumeroBoleto());
            boletoResponse.setFechaSalida(dateFormat.format(boleto.getHorario().getFechaSalida()) + " " +
                    (boleto.getHorario().getHoraSalida().toString().substring(0, boleto.getHorario().getHoraSalida().toString().length() - 3)));
            boletoResponse.setDestino(boleto.getHorario().getDestino().getNombre());
            return boletoResponse;
        } catch (Exception e) {
            e.printStackTrace();
            return new BoletoResponse();
        }
    }

}
