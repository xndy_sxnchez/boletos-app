/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.repository;

import com.lsi.boleteria.entidades.Boleto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 *
 * @author Juan Carlos
 */
public interface BoletoRepository extends CrudRepository<Boleto, Long> {

    Boleto findByAsientoIdAndHorarioId(Long idAsiento, Long idHorario);

    Boleto findByNumeroBoleto(Long numeroBoleto);

    List<Boleto> findAllByCliente_Id(Long idCliente);
    
}
