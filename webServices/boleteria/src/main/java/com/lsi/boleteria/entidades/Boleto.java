/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.entidades;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Juan Carlos
 */
@Entity
@Table(name = "boleto", schema = "aplicacion")
public class Boleto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "numero_boleto")
    private Long numeroBoleto;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "numero_boleto_formato")
    private String numeroBoletoFormato;
    @JoinColumn(name = "cliente", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Cliente cliente;
    @Column(name = "fecha_compra")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCompra;
    @Column(name = "fecha_salida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSalida;
    @JoinColumn(name = "asiento", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Asiento asiento;
    @JoinColumn(name = "horario", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Horario horario;

    public Boleto() {
    }

    public Boleto(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public Asiento getAsiento() {
        return asiento;
    }

    public void setAsiento(Asiento asiento) {
        this.asiento = asiento;
    }

    public Horario getHorario() {
        return horario;
    }

    public void setHorario(Horario horario) {
        this.horario = horario;
    }

    public Long getNumeroBoleto() {
        return numeroBoleto;
    }

    public void setNumeroBoleto(Long numeroBoleto) {
        this.numeroBoleto = numeroBoleto;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }   

    public String getNumeroBoletoFormato() {
        return numeroBoletoFormato;
    }

    public void setNumeroBoletoFormato(String numeroBoletoFormato) {
        this.numeroBoletoFormato = numeroBoletoFormato;
    }

    @Override
    public String toString() {
        return "com.lsi.boleteria.entidades.Boleto[ id=" + id + " ]";
    }
    
}
