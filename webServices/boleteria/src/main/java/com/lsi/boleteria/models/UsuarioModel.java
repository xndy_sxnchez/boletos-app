package com.lsi.boleteria.models;

import com.lsi.boleteria.configs.Constantes;

public class UsuarioModel {

    private Long idUsuario;
    private String nombresCompletos;
    private String nombreUsuario;
    private String estado;
    private String mensaje;
    private String correo;

    public UsuarioModel() {
    }

    public UsuarioModel(String estado, String mensaje) {
        this.estado = estado;
        this.mensaje = mensaje;
    }

    public UsuarioModel(Long idUsuario, String nombresCompletos, String nombreUsuario, String correo) {
        this.idUsuario = idUsuario;
        this.nombresCompletos = nombresCompletos;
        this.nombreUsuario = nombreUsuario;
        this.correo = correo;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombresCompletos() {
        return nombresCompletos;
    }

    public void setNombresCompletos(String nombresCompletos) {
        this.nombresCompletos = nombresCompletos;
    }
}
