/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.repository;

import com.lsi.boleteria.entidades.Usuario;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Juan Carlos
 */
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Usuario findByNombreUsuarioAndContrasenaAndEstado(String nombreUsuario, String contrasenia, Boolean estado);


    Usuario findByNombreUsuario(String nombreUsuario);

}
