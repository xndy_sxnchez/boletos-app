/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lsi.boleteria.repository;

import com.lsi.boleteria.entidades.FormatoNotificacion;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author gutya
 */
public interface FormatoNotificacionRepository extends CrudRepository<FormatoNotificacion, Long>{
    
}
